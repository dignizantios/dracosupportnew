//
//  ServicePostFix.swift
//  Draco Support
//
//  Created by Haresh on 26/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation

let kBasicUsername = "root"
let kBasicPassword = "123"

//let basicAdminURL = "http://support.dracofs.com:8080/draco-support/admin/"
let basicAdminURL = "http://support.dracofs.com:8080/draco-support/v2/admin/"

//MARK: - User URLs
let userURL = "user/"

let registerURL = "register"
let adminListURL = "organization"
let loginURL = "login"
let forgotPasswordURL = "forgot_password"
let logoutURL = "logout"


//MARK: - Issue DetailsURLs
let issueDetailURL = "IssueDetail/"

let submitDetailsURL = "submitRequest"
let requesListURL = "RequestList"
let finishIssueURL = "finishIssue"
let startIssueURL = "confirmIssue"
let QRCodeSetupURL = "QR_setup"
let needAssistURL = "needAssist"
let QRCodeDetails = "QR_codeDetail"
let DeleteDataRequest = "deleteRequest"
let batchList = "batchlist"
let inProgress = "Inprogress"
let technicianList = "technicianlist"
let issueReceived = "issueReceived"
let updateProblemdesc = "updateProblemdesc"
let technicianlist = "technicianlist"

let notificationlist = "notificationlist"
let remove_notification = "remove_notification"
let notificationclick = "notificationclick"
let unseen_notification = "unseen_notification"



//MARK: - Reports URLs

let reportURL = "reports/"
let historyListURL = "Reportlist"
let addReportURL = "addReport"

//MARK: - Client URL

let clientURL = "client/"
//let basicURLForClient = "http://support.dracofs.com:8080/draco-support/client/"
let basicURLForClient = "http://support.dracofs.com:8080/draco-support/v2/client/"
