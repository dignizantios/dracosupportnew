//
//  CommonDateTimeVC.swift
//  Draco Support
//
//  Created by YASH on 25/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

public protocol DateTimePickerDelegate
{
    func setDateandTime(dateValue : Date,type : Int)
}

class CommonDateTimeVC: UIViewController
{
    
    //MARK: - Outlet
    
    @IBOutlet weak var vwColor: UIView!
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    //MARK: - Variable
    
    var pickerDelegate : DateTimePickerDelegate?
    var controlType : Int = 0 //0 = Time / 1 = Date / 2 = Datetime
    
    var setMinimumDate : Date?
    var setMaximumDate : Date?
    
    var isSetMinimumDate : Bool = false
    var isSetMaximumDate : Bool = false
    
    var isSetDatePickerDate = false
    var datePickerDateValue : Date?
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        vwColor.backgroundColor = UIColor.appthemeRedColor
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
        if controlType == 0
        {
            dateTimePicker.datePickerMode = .time
        }
        else if controlType == 1
        {
            dateTimePicker.datePickerMode = .date
        }
        else
        {
            dateTimePicker.datePickerMode = .dateAndTime
        }
        
        if(isSetMinimumDate)
        {
            dateTimePicker.minimumDate = setMinimumDate

        }
        
        if(isSetMaximumDate)
        {
            dateTimePicker.maximumDate = setMaximumDate
        }
        
        if(isSetDatePickerDate)
        {
            dateTimePicker.date = datePickerDateValue!
        }
        
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnDoneTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setDateandTime(dateValue: dateTimePicker.date, type: controlType)
    }
    
    
    @IBAction func btnCancelTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

}
