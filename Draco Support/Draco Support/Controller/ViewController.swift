//
//  ViewController.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblDetailsMsg: UILabel!
    @IBOutlet weak var btnAdmin: UIButton!
    
    @IBOutlet weak var btnWithoutQRCode: UIButton!
    @IBOutlet weak var txtQRCodeEnter: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet var lblSubmitRequest: UILabel!
    
    @IBOutlet weak var vwScanner: QRCodeReaderView!{
        didSet {
            vwScanner.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = false
                
               // $0.rectOfInterest         = CGRect(x: 0.0, y: 0.0, width: 0.6, height: 0.6)
            })
        }
    }
    
     lazy var reader: QRCodeReader = QRCodeReader()
    
    //MARK: - Variable
    
    var arrayCompanyList : [JSON] = []
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        //CheckPermission
        guard checkScanPermissions(), !reader.isRunning else { return }
        reader.didFindCode = { result in
            print("Completion with result: \(result.value) of type \(result.metadataType)")
            
            print("result:\(result)")
            
            let scanDataString = result.value
            
            self.isQRCodeIsValidOrNot(qrCode: scanDataString)
//            self.checkQRRegisterOrNot(qrString: scanDataString)
           
            NotificationCenter.default.addObserver(self, selector: #selector(self.setUpBadgeCountAndBarButton), name: Notification.Name("notificationGet"), object: nil)
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        isScanScreenForIssue = true
        
//        getCompanyOrganizationList()
        
        reader.startScanning()
        
        if getUserDetail("role") == "3" {
             self.navigationController?.navigationBar.isHidden = false
            self.setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Draco_Support_key"), isRightButtonHidden: false)
            setUpBadgeCountAndBarButton()
           
        }
        else {
            self.navigationController?.navigationBar.isHidden = false
            self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
            
//            self.navigationController?.navigationBar.isHidden = false
        }
        
//        let rightButton = UIBarButtonItem(title: getCommonString(key: "Admin_key"), style: .plain, target: self, action: #selector(redirectToAdmin))
//        rightButton.tintColor = UIColor.white
//        self.navigationItem.rightBarButtonItem = rightButton
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isScanScreenForIssue = false
        reader.stopScanning()
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationGet"), object: nil)
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    
}

//MARK: - SetupUI

extension ViewController
{
    func setupUI()
    {
        
        print("All ViewController:\(self.navigationController?.viewControllers)")
        
        self.vwMain.backgroundColor = UIColor.appThemeDarkGrayColor
        
        self.lblDetailsMsg.text = getCommonString(key: "Please_scan_the_QR_code_on_your_device_OR_key")
        self.lblSubmitRequest.text = getCommonString(key: "No_QR_code_Submit_a_request_key")
        
        [lblDetailsMsg, lblSubmitRequest].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        txtQRCodeEnter.delegate = self
        txtQRCodeEnter.placeholder = getCommonString(key: "Enter_QR_Code_key")
        txtQRCodeEnter.font = themeFont(size: 16, fontname: .regular)
        txtQRCodeEnter.textColor = UIColor.black
        txtQRCodeEnter.tintColor = UIColor.appthemeRedColor
        txtQRCodeEnter.backgroundColor = UIColor.white
        
        btnAdmin.setThemeButtonUI()
        btnAdmin.setTitle(getCommonString(key: "Admin_key"), for: .normal)
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        btnWithoutQRCode.setThemeButtonUI()
        btnWithoutQRCode.setTitle(getCommonString(key: "Without_scan_QR_code_key"), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtQRCodeEnter)
        
    }
    
    @objc func setUpBadgeCountAndBarButtonn() {
        
        setUpBadgeCountAndBarButton()
    }
}

//MARK: - IBAction method

extension ViewController
{
    @IBAction func btnAdminTapped(_ sender: UIButton) {
        
        /*
        if getUserDetail("id") != ""
        {
            appdelgate.objCustomTabBar = CustomTabbarVC()
            self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
            
        }
        else
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminLoginVc") as! AdminLoginVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        */
    }
    
    @objc func redirectToAdmin()
    {
        if getUserDetail("id") != ""
        {
            appdelgate.objCustomTabBar = CustomTabbarVC()
            self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
            
        }
        else
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminLoginVc") as! AdminLoginVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if (txtQRCodeEnter.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_QR_code_key"))
        }
        else if (txtQRCodeEnter.text?.isNumeric == false)
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_QR_code_key"))
        }
        else
        {
            self.isQRCodeIsValidOrNot(qrCode: self.txtQRCodeEnter.text ?? "")
//            self.checkQRRegisterOrNot(qrString: txtQRCodeEnter.text!)
        }
        
    }
    
    
    @IBAction func btnWithoutQRCodeTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SubmitDetailsVc") as! SubmitDetailsVc
        obj.selectedController = .withoutScan
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    
    //MARK: - Other Action
    
    func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    func redirectToSubmitDetails(qrScanData : JSON)
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SubmitDetailsVc") as! SubmitDetailsVc
        obj.selectedController = .withScanOrNumber
        obj.scanData = qrScanData
        self.txtQRCodeEnter.text = ""
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func checkQRRegisterOrNot(qrString:String)
    {
        var jsonofScanQR = JSON()
        
        if self.arrayCompanyList.contains(where: { (json) -> Bool in
            
            if json["qr_code"].stringValue == qrString
            {
                jsonofScanQR = json
                return true
            }
            return false
        })
        {
            self.redirectToSubmitDetails(qrScanData: jsonofScanQR)
        }
        else
        {
            makeToast(strMessage: getCommonString(key: "QRCode_is_not_registerrd_key"))
            self.reader.startScanning()
        }
    }
    
}

//MARK: - TextFIeld Delegate

extension ViewController :UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

// MARK: - QRCodeReader Delegate Methods

extension ViewController
{
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
        
    }

}


//MARK: - API calling

extension ViewController {
    
    
    func isQRCodeIsValidOrNot(qrCode: String) {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(basicAdminURL)\(issueDetailURL)\(QRCodeDetails)"
            
            print("URL: \(url)")
            
            let param = ["lang":"0",
                         "qr_code":qrCode,
                         "company_id":getUserDetail("company_id")]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                self.reader.startScanning()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        self.redirectToSubmitDetails(qrScanData: json["data"])
                        
//                        let data = json["data"].arrayValue
//
//                        self.arrayCompanyList = []
//
//                        self.arrayCompanyList = data
//                        makeToast(strMessage: json["msg"].stringValue)
                        print("self.arrayCompanyList:- \(self.arrayCompanyList)")
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        // self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    func getCompanyOrganizationList()
    {
       
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(QRCodeDetails)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                self.reader.startScanning()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyList = []
                        
                        self.arrayCompanyList = data
                        print("self.arrayCompanyList:- \(self.arrayCompanyList)")
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
}

