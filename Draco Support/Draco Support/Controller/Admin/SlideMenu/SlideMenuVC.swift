//
//  SlideMenuVC.swift
//  Draco Support
//
//  Created by Khushbu on 26/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController

class SlideMenuVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var lblAdmin: UILabel!
    @IBOutlet var lblAdminName: UILabel!
    @IBOutlet var lblAdminEmail: UILabel!
    @IBOutlet var tblSideMenu: UITableView!
    
    //MARK: Variables
    var arrayImg = NSArray()
    var arrayTitle = NSArray()

    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        
    }
    
    //status bar color change
    override func viewWillAppear(_ animated: Bool) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView { statusBar.backgroundColor = UIColor.appthemeRedColor }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView { statusBar.backgroundColor = UIColor.clear }
//        self.navigationController?.navigationBar.isHidden = true
    }
    
}

//MARK: setUpUI
extension SlideMenuVC {
    
    func setUpUI() {
        
//        self.navigationController?.navigationBar.isHidden = true
        
        tblSideMenu.allowsSelection = true
        tblSideMenu.isUserInteractionEnabled = true
        
        tblSideMenu.tableFooterView = UIView()
        
        [lblAdmin].forEach { (lbl) in
            lbl?.textColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
            lbl?.font = themeFont(size: 20, fontname: .bold)
        }
        
        [lblAdminName].forEach { (lbl) in
            lbl?.textColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
            lbl?.font = themeFont(size: 18, fontname: .bold)
        }
        
        [lblAdminEmail].forEach { (lbl) in
            lbl?.textColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        if getUserDetail("role") == "1" {
            
            self.lblAdmin.text = getCommonString(key: "Super_admin_key")
            self.lblAdminName.text = "\(getUserDetail("firstname") ) \(getUserDetail("lastname"))"
            self.lblAdminEmail.text = getUserDetail("email")
            
            arrayImg = ["ic_qr_code_white_icon", "ic_logout"]
            arrayTitle = [getCommonString(key: "Add_Modify_a_QR_code_key"), getCommonString(key: "Logout")]
        }
        if getUserDetail("role") == "2" {
            
            self.lblAdmin.text = getCommonString(key: "Admin_key")
            self.lblAdminName.text = "\(getUserDetail("firstname") ) \(getUserDetail("lastname"))"
            self.lblAdminEmail.text = getUserDetail("email")
            
            arrayImg = ["ic_qr_code_white_icon", "ic_ticket", "ic_logout"]
            arrayTitle = [getCommonString(key: "Add_Modify_a_QR_code_key"), getCommonString(key: "Submit_a_ticket_key"), getCommonString(key: "Logout")]
        }
    }
}

//MARK: SideMenuNavigationController
extension SlideMenuVC {
    
}

//MARK: TableView Datasource/Delegate
extension SlideMenuVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if getUserDetail("role") == "1" {
            return self.arrayImg.count
        }
        if getUserDetail("role") == "2" {
            return self.arrayImg.count
        }
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuTableCell", for: indexPath) as! SlideMenuTableCell
        
        if getUserDetail("role") == "1" {
            
            cell.imgIcon.image = UIImage(named: arrayImg[indexPath.row] as? String ?? "")
            cell.lblTitle.text = arrayTitle[indexPath.row] as? String
        }
        if getUserDetail("role") == "2" {
            
            cell.imgIcon.image = UIImage(named: arrayImg[indexPath.row] as? String ?? "")
            cell.lblTitle.text = arrayTitle[indexPath.row] as? String
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if getUserDetail("role") == "1" {
            
            if indexPath.row == 0 {
                let qrCodeSetup = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanForCodeSetup") as! ScanForCodeSetup
                
                SJSwiftSideMenuController.hideLeftMenu()
                SJSwiftSideMenuController.pushViewController(qrCodeSetup, animated: false)
//                self.navigationController?.pushViewController(qrCodeSetup, animated: false)
            }
            else if indexPath.row == 1 {
                SJSwiftSideMenuController.hideLeftMenu()
                self.LogoutButtonAction()
            }
        }
        else {
            if indexPath.row == 0 {
                
                let qrCodeSetup = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanForCodeSetup") as! ScanForCodeSetup
                
                SJSwiftSideMenuController.hideLeftMenu()
                SJSwiftSideMenuController.pushViewController(qrCodeSetup, animated: false)
//                self.navigationController?.pushViewController(qrCodeSetup, animated: false)
//                QRCodeSetupScreen()
            }
            else if indexPath.row == 1 {
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                
                SJSwiftSideMenuController.hideLeftMenu()
                SJSwiftSideMenuController.pushViewController(obj, animated: false)
//                self.navigationController?.pushViewController(obj, animated: true)
            }
            else {
                SJSwiftSideMenuController.hideLeftMenu()
                self.LogoutButtonAction()
            }
        }
        
    }
    
}

//MARK:
extension SlideMenuVC {
    
}
