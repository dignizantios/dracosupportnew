//
//  SlideMenuTableCell.swift
//  Draco Support
//
//  Created by Khushbu on 26/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SlideMenuTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
