//
//  NotificationTableCell.swift
//  Draco Support
//
//  Created by Khushbu on 27/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class NotificationTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var vwBG: CustomView!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var lblTitleNotifications: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTitleNotifications].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        [lblDate].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
