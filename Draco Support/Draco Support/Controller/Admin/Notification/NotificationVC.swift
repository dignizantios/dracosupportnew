//
//  NotificationVC.swift
//  Draco Support
//
//  Created by Khushbu on 27/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class NotificationVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var tblNotifications: UITableView!
    @IBOutlet var vwIndicatorHeight: NSLayoutConstraint!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    
    //MARK: Variables
    var arrayNotification : [JSON] = []
    var strErrorMessage = ""
    var nextOffset : Int = 0
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGrayColor
        
        return refreshControl
    }()
    
    
    //MARK: Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        isRequestList = true
        setUpUI()
        
        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Notification_key"))
        self.getNotificationAPICalling(isShowLoader: isRequestList, nextOffSet: 0)
        self.viewWillAppear(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        GlobalVariables.notificationCount = "0"
        appdelgate.uncheckedNotificationAPICalling()
        //        self.navigationRightBtnSet()

    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//        self.viewWillAppear(true)
//
//        GlobalVariables.notificationCount = "0"
//        appdelgate.uncheckedNotificationAPICalling()
//        self.navigationRightBtnSet()
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
}


//MARK: SetUpUI
extension NotificationVC {
    
    func setUpUI() {
        
        self.vwIndicatorHeight.constant = 0
        self.indicator.isHidden = true
        
        self.tblNotifications.register(UINib(nibName: "NotificationTableCell", bundle: nil), forCellReuseIdentifier: "NotificationTableCell")
        self.tblNotifications.tableFooterView = UIView()
        tblNotifications.addSubview(self.refreshControl)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.arrayNotification = []
        getNotificationAPICalling(isShowLoader: isRequestList, nextOffSet: 0)
        refreshControl.endRefreshing()
    }
    
    func navigationRightBtnSet() {
        
        let btnClearAll = UIBarButtonItem(title: getCommonString(key: "Clear_all_key"), style: .plain, target: self, action: #selector(btnClearAllAction))
        btnClearAll.tintColor = .white
        
        if self.arrayNotification.count > 0 {
            navigationItem.rightBarButtonItem = btnClearAll
        }
        
    }
    
    @objc func btnClearAllAction() {
        
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_you_want_to_delete_all_notification?"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.deleteDataAPICalling(isShowLoader: isRequestList, historyId: "0", selectedRow: -1)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
//        self.deleteDataAPICalling(isShowLoader: isRequestList, historyId: "0", selectedRow: nil)
    }
}


//MARK: TableView Delegate/Datasource
extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayNotification.count == 0 {

            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = .center
            lbl.textColor = UIColor.appthemeRedColor
            lbl.center = tblNotifications.center
            tableView.backgroundView = lbl
            
            return 0
        }
        
        tableView.backgroundView = nil
        return self.arrayNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell", for: indexPath) as! NotificationTableCell
        
        if self.arrayNotification.count != 0 {
            
            let dict = self.arrayNotification[indexPath.row]
            
            cell.lblTitleNotifications.text = dict["message"].stringValue
            
            let date = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "MMMM d, yyyy h:mm a", strDate: dict["created_date"].stringValue)
            cell.lblDate.text = date
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.arrayNotification.count-1 {
            if self.nextOffset != -1 {
                self.vwIndicatorHeight.constant = 60
                self.indicator.isHidden = false
                self.getNotificationAPICalling(isShowLoader: isRequestList, nextOffSet: self.nextOffset)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if getUserDetail("role") != "3" {
            
            let data = self.arrayNotification[indexPath.row]
            self.showDetailAPICalling(isShowLoader: isRequestList, historyId: data["issue_id"].stringValue)
        }
        
    }
    
    
    @objc func btnDeleteAction(_ sender: UIButton) {
        
        let data = self.arrayNotification[sender.tag]
        
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_you_want_to_delete_this_notification?"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.deleteDataAPICalling(isShowLoader: isRequestList, historyId: data["tbl_notification_history_id"].stringValue, selectedRow: sender.tag)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}


//MARK: ApiSetUp
extension NotificationVC {
    
    @objc func getNotificationAPICalling(isShowLoader:Bool, nextOffSet: Int)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(notificationlist)"
            
            print("URL: \(url)")
            
            let param : [String : Any] = ["user_id" : getUserDetail("id"),
                         "offset":nextOffSet]
            
            print("param :\(param)")
            
            if isShowLoader {
                if nextOffSet == 0 {
                    self.arrayNotification = []
                    self.showLoader()
                }
            }
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader
                {
                    self.hideLoader()
                }
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"].arrayValue
                        
                        self.vwIndicatorHeight.constant = 0
                        self.indicator.isHidden = true
                        
                        self.nextOffset = json["next_offset"].intValue
                        self.arrayNotification += data
                        
                        DispatchQueue.main.async {
                            self.navigationRightBtnSet()
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        self.arrayNotification = []
                        self.tblNotifications.reloadData()
                        self.strErrorMessage = json["msg"].stringValue
                    }
                    
                    self.tblNotifications.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    @objc func deleteDataAPICalling(isShowLoader:Bool, historyId: String, selectedRow: Int) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(userURL)\(remove_notification)"
            
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("id"),
                         "tbl_notification_history_id":historyId]
            
            print("param :\(param)")
            
            if isShowLoader {
                self.showLoader()
            }
            
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader {
                    self.hideLoader()
                }
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"].arrayValue
                        
                        if selectedRow == -1 {
                            self.arrayNotification = []
                        }
                        else {
                            self.arrayNotification.remove(at: selectedRow)
                        }
                        self.tblNotifications.reloadData()
                        print("Data:",data)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        self.strErrorMessage = json["msg"].stringValue
                    }
//                    self.tblNotifications.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func showDetailAPICalling(isShowLoader:Bool, historyId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(notificationclick)"
            
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("id"),
                         "issue_id":historyId,
                         "access_token":getUserDetail("access_token")]
            
            print("param :\(param)")
            
            if isShowLoader {
                self.showLoader()
            }
            
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader {
                    self.hideLoader()
                }
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        let data = json["data"].arrayValue
                        let dataHistory = json["dataHistory"].arrayValue
                        
                        if data.count > 0 {
                            
                            let detailVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RequestDetailsvc") as! RequestDetailsvc
                            
                            for i in 0..<data.count {
                                detailVC.dictIssueData = data[i]
                            }
                            
                            self.navigationController?.pushViewController(detailVC, animated: false)
                        }
                        else if dataHistory.count > 0 {
                            
                            let reportDetailVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminReportDetailsVC") as! AdminReportDetailsVC
                            
                            for i in 0..<dataHistory.count {
                                reportDetailVC.dictReportData = dataHistory[i]
                            }
                            
                            self.navigationController?.pushViewController(reportDetailVC, animated: true)
                            
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        self.strErrorMessage = json["msg"].stringValue
                    }
                    //                    self.tblNotifications.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
}


//MARK:
extension NotificationVC {
    
}
