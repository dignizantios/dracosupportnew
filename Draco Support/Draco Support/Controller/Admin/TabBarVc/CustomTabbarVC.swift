//
//  CustomTabbarVC.swift
//  FrankPorter
//
//  Created by YASH on 28/08/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class CustomTabbarVC: UITabBarController,UITabBarControllerDelegate {
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        let screenWidth = UIScreen.main.bounds.width
        let tabBarHeight = self.tabBar.frame.size.height
        
        print("tab bar height - ",self.tabBar.frame.size.height)
        let new = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RequestListVc") as! RequestListVc
        let tabbarOneItem = UITabBarItem(title:getCommonString(key: "New_Key"), image: UIImage(named:"BG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named:"whiteBG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        tabbarOneItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+10);
        new.tabBarItem = tabbarOneItem
        
//        print("tab bar height - ",self.tabBar.frame.size.height)
//        let request = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RequestListVc") as! RequestListVc
//        let tabbarOneItem = UITabBarItem(title:getCommonString(key: "Request_key"), image: UIImage(named:"BG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named:"whiteBG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
//        tabbarOneItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+10);
//        //        tabbarOneItem.setTitleTextAttributes([NSAttributedStringKey.font: themeFont(size: 16, fontname: .Boook)], for: .normal)
//        request.tabBarItem = tabbarOneItem
        
        let history = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "HistoryVc") as! HistoryVc
        let tabbarTwoItem = UITabBarItem(title:getCommonString(key: "History_key"), image: UIImage(named:"BG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named:"whiteBG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        tabbarTwoItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+10);
        
        history.tabBarItem = tabbarTwoItem
        
        let inProgress = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "InProgressVC") as! InProgressVC
        let tabBarThreeItem = UITabBarItem(title: getCommonString(key: "In_Progress_Key"), image: UIImage(named: "BG.png")?.imageWithImage(newWidth: screenWidth/3, height: tabBarHeight).withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "whiteBG.png")?.imageWithImage(newWidth: screenWidth/3, height: tabBarHeight).withRenderingMode(.alwaysOriginal))
        tabBarThreeItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+10)
        
        inProgress.tabBarItem = tabBarThreeItem
        
//        let qrCodeSetup = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanForCodeSetup") as! ScanForCodeSetup
//        let tabbarThreeItem = UITabBarItem(title:getCommonString(key: "QR_Code_Setup_key"), image: UIImage(named:"BG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named:"whiteBG.png")?.imageWithImage(newWidth: screenWidth/3,height:tabBarHeight).withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
//        tabbarThreeItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+10);
//
//        qrCodeSetup.tabBarItem = tabbarThreeItem

//        let n1 = UINavigationController(rootViewController:new)
//        let n2 = UINavigationController(rootViewController:history)
//        let n3 = UINavigationController(rootViewController:qrCodeSetup)
        
        let n1 = UINavigationController(rootViewController: new)
        let n2 = UINavigationController(rootViewController: inProgress)
        let n3 = UINavigationController(rootViewController: history)
        
        
        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
//        n3.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false

        self.viewControllers = [n1,n2,n3]
//        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.white
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: themeFont(size: 15, fontname: .regular),
            ] as [NSAttributedString.Key : Any]
        
        let attributesSelected = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: themeFont(size: 15, fontname: .regular),
            ] as [NSAttributedString.Key : Any]
        
        UITabBarItem.appearance().setTitleTextAttributes(attributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected, for: .selected)
        
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.appThemeDarkGrayColor.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
//        if #available(iOS 10.0, *) {
//            self.tabBar.unselectedItemTintColor = .white
//        } else {
//            // Fallback on earlier versions
//        }
        
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
      
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard
            let tab = tabBarController.viewControllers?.index(of: viewController), [2].contains(tab)
            else {
                if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                    vc.dismiss(animated: true, completion: nil)
                    vc.popToRootViewController(animated: false)
                }
                return true
        }
        return true
    }
    
}


extension UIImage {
    
    func imageWithImage( newWidth: CGFloat,height:CGFloat) -> UIImage
    {
        let newSize = CGSize(width: newWidth-1, height: height+10)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 10), size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
