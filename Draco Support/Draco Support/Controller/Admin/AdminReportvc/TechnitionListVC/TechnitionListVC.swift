//
//  TechnitionListVC.swift
//  Draco Support
//
//  Created by Khushbu on 23/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class TechnitionListVC: UIViewController {

    
    //MARK: Outlets
    @IBOutlet var vwBGHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtSearchName: CustomTextField!
    @IBOutlet var tblTechnicianName: UITableView!
    @IBOutlet var tblTechnitionHeight: NSLayoutConstraint!
    
    
    //MARK: Variables
    var arrayData = [JSON]()
    var arraySearchData = [JSON]()
    
    var addTechnition : (JSON) -> Void = { _ in }
    var arrayFilter : ([JSON]) -> Void = { _ in}
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        arraySearchData = arrayData
        self.tblTechnicianName.reloadData()
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
}


//MARK: SetUpUI
extension TechnitionListVC {
    
    func setUpUI() {
        
        let screenHeight = (UIScreen.main.bounds.height-130)
        let height = tblTechnicianName.contentSize.height
        self.tblTechnicianName.isScrollEnabled = true
        
        if height > screenHeight {
            self.tblTechnitionHeight.constant = screenHeight
        }
        else {
            self.tblTechnitionHeight.constant = tblTechnicianName.contentSize.height
        }
        
        
        self.lblTitle.text = getCommonString(key: "Technition_Key")
        self.lblTitle.font = themeFont(size: 18, fontname: .regular)
        self.lblTitle.textColor = UIColor.appThemeDarkGrayColor
        
        [txtSearchName].forEach { (txt) in
            txt?.setThemeTextFieldUI()
            txt?.placeholder = getCommonString(key: "Search_here_key")
        }
    }
}


//MARK: TextField Delegate
extension TechnitionListVC : UITextFieldDelegate {
    
    @IBAction func txtSearchEditingChanged(_ sender: UITextField) {
        
        guard let text = (self.txtSearchName.text) else { return }
        
        if text == "" {
            self.arrayData = self.arraySearchData
        }
        else {
            
            self.arrayData = []
            let dicSearch = self.arraySearchData.filter({$0["technicianname"].stringValue.trimmed().lowercased().replacingOccurrences(of: " ", with: " ").contains((text).lowercased())})
            
            dicSearch.forEach({
                self.arrayData.append($0)
            })
        }
        self.tblTechnicianName.reloadData()
    }
    
}


//MARK: TableView Delegate/Datasource
extension TechnitionListVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TechnitionListTableCell", for: indexPath) as! TechnitionListTableCell
        
        let dict = self.arrayData[indexPath.row]
        
        if dict["isSelected"].stringValue == "0" {
            
            cell.lblTechnicianName.text = dict["technicianname"].stringValue
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dicSelected = self.arrayData[indexPath.row]
        dicSelected["isSelected"].stringValue = "1"
        
        self.addTechnition(dicSelected)
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        if UIScreen.main.bounds.height < 321 {
//            self.tblTechnitionHeight.constant >= 417.0 ? 417.0 : CGFloat(self.tblTechnitionHeight.constant)
//        }
//        else {
//            self.tblTechnitionHeight.constant >= 480.0 ? 480.0 : CGFloat(self.tblTechnitionHeight.constant)
//        }
        
//        self.tblTechnitionHeight.constant = tableView.contentSize.height
    }
    
    func setViewInCenter() {
        
    }
    
}


//MARK: Button Action
extension TechnitionListVC {
    
    @IBAction func btnDismissAction(_ sender: Any) {
        
        self.arrayFilter(self.arrayData)
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func btnDissmisAction(_ sender: Any) {
        self.arrayFilter(self.arrayData)
        self.dismiss(animated: false, completion: nil)
    }

}
