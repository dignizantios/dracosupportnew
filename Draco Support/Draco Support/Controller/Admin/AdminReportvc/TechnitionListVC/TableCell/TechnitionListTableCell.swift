//
//  TechnitionListTableCell.swift
//  Draco Support
//
//  Created by Khushbu on 23/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TechnitionListTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var lblTechnicianName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTechnicianName].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
