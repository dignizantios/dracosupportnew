//
//  AdminReportVC.swift
//  Draco Support
//
//  Created by Haresh Bhai on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import DropDown

class AdminReportVC: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblTitleCompanyName: UILabel!
    @IBOutlet weak var lblTitleTechName: UILabel!
    @IBOutlet weak var lblTitleClientName: UILabel!
    @IBOutlet weak var lblTitleDate: UILabel!
    @IBOutlet weak var lblTitleProblem: UILabel!
    @IBOutlet weak var lblTitleServiceType: UILabel!
    @IBOutlet weak var lblTitleServiceDate: UILabel!
    @IBOutlet weak var lblTitleTimeSpent: UILabel!
    @IBOutlet weak var lblTitleServiceDetails: UILabel!
    @IBOutlet weak var lblTitleParts: UILabel!
    @IBOutlet weak var lblTitleNotes: UILabel!
    
    @IBOutlet weak var txtCompanyName: CustomTextField!
    @IBOutlet weak var viewCompanyName: CustomView!
    
    @IBOutlet weak var txtTechnicianName: CustomTextField!
    @IBOutlet weak var viewTechnicianName: CustomView!
    @IBOutlet var btnTechnicianName: UIButton!
    @IBOutlet var tblTechnitionName: UITableView!
    
    @IBOutlet weak var txtClientName: CustomTextField!
    @IBOutlet weak var viewClientName: CustomView!
    @IBOutlet var tblHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtDate: CustomTextField!
    @IBOutlet weak var viewDate: CustomView!
    
    @IBOutlet weak var txtvwProblemDescription: CustomTextview!
    @IBOutlet weak var lblProblemDescriptionPlaceHolder: UILabel!
    @IBOutlet weak var vwProblemDescription: CustomView!
    
    @IBOutlet weak var txtServiceType: CustomTextField!
    @IBOutlet weak var viewServiceType: CustomView!
    
    @IBOutlet weak var txtServiceDate: CustomTextField!
    @IBOutlet weak var viewServiceDate: CustomView!
    
    @IBOutlet weak var txtTimeSpent: CustomTextField!
    @IBOutlet weak var viewTimeSpent: CustomView!
    
    @IBOutlet weak var txtvwServiceDetailsDescription: CustomTextview!
    @IBOutlet weak var lblServiceDetailsDescriptionPlaceHolder: UILabel!
    @IBOutlet weak var vwServiceDetailsDescription: CustomView!

    @IBOutlet weak var txtvwPartsDescription: CustomTextview!
    @IBOutlet weak var lblPartsDescriptionPlaceHolder: UILabel!
    @IBOutlet weak var vwPartsDescription: CustomView!
    
    @IBOutlet weak var txtvwNotesRequestDescription: CustomTextview!
    @IBOutlet weak var lblNotesRequestDescriptionPlaceHolder: UILabel!
    @IBOutlet weak var vwNotesRequestDescription: CustomView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK : - Variable
    
    var isServiceDate = false
    
    var date = ""
    var serviceDate = ""
    
    var selectedDate = Date()
    
    var dictData = JSON()
    
    var serviceTypeDD = DropDown()
    var serviceTypeID = 0
    var arrayTechnicianName = [JSON]()
    var filterArray = [JSON]()
    var requstDate = ""
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerXib()
        technicianList()
        self.setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        self.configDD(dropdown: serviceTypeDD, sender: txtServiceType)
        selectionIndex()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Report_key"))
    }
    
    //MARK: - Setup UI
    
    func setupUI() {
        
        self.arrayTechnicianName = ["vishal parmar", "abhay pansora", "jaydeep virani"]
        
        [txtCompanyName,txtTechnicianName, txtClientName, txtDate, txtServiceType, txtServiceDate, txtTimeSpent].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 15
            txt?.setThemeTextFieldUI()
        }
        
        [viewCompanyName,viewTechnicianName, viewClientName, viewDate, viewServiceType, viewServiceDate, viewTimeSpent].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        [vwProblemDescription, vwServiceDetailsDescription, vwPartsDescription, vwNotesRequestDescription].forEach { (vw) in
            vw?.cornerRadius = 5
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        [viewTechnicianName].forEach { (vw) in
            vw?.cornerRadius = 25
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.white
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }

        [txtvwProblemDescription, txtvwServiceDetailsDescription, txtvwPartsDescription, txtvwNotesRequestDescription].forEach { (txt) in
            txt?.delegate = self
            txt?.tintColor = UIColor.appthemeRedColor
            txt?.font = themeFont(size: 16, fontname: .regular)
        }
        
        [lblProblemDescriptionPlaceHolder,lblServiceDetailsDescriptionPlaceHolder,lblPartsDescriptionPlaceHolder,lblNotesRequestDescriptionPlaceHolder].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_here_key")
        }
        [lblTitleCompanyName,lblTitleTechName,lblTitleClientName,lblTitleDate,lblTitleProblem,lblTitleServiceType,lblTitleServiceDate,lblTitleTimeSpent,lblTitleServiceDetails,lblTitleParts,lblTitleNotes].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        self.lblTitleCompanyName.text = getCommonString(key: "Company_Name_key")
        self.lblTitleTechName.text = getCommonString(key: "Technician_Name_key")
        self.lblTitleClientName.text = getCommonString(key: "Client_Name_key")
        self.lblTitleDate.text = getCommonString(key: "Request_Date_Key")
        self.lblTitleProblem.text = getCommonString(key: "Problem_key")
        self.lblTitleServiceType.text = getCommonString(key: "Service_Type_key")
        self.lblTitleServiceDate.text = getCommonString(key: "Service_Date_key")
        self.lblTitleTimeSpent.text = getCommonString(key: "Time_Spent_key")
        self.lblTitleServiceDetails.text = getCommonString(key: "Service_Details_key")
        self.lblTitleParts.text = getCommonString(key: "Parts_key")
        self.lblTitleNotes.text = getCommonString(key: "Notes_Request_key")
        
        txtTechnicianName.placeholder = getCommonString(key: "Enter_here_key")
        txtTimeSpent.placeholder = getCommonString(key: "Enter_here_key") + getCommonString(key: "(hours)_key")
        
        [txtDate,txtServiceDate,txtServiceType].forEach { (txt) in
            txt?.placeholder = getCommonString(key: "Select_here_key")
        }
        
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)

        addDoneButtonOnKeyboard(textfield: txtTimeSpent)
        
        txtCompanyName.text = getUserDetail("company_name")
        txtCompanyName.isUserInteractionEnabled = false
        txtTechnicianName.text = "\(getUserDetail("firstname")) \(getUserDetail("lastname"))"
//        txtTechnicianName.text = "\(dictData["tec_firstname"]) \(dictData["tec_lastname"])"
        txtTechnicianName.isUserInteractionEnabled = false
        txtClientName.text = dictData["client_name"].stringValue
        txtClientName.isUserInteractionEnabled = false
//        [txtTechnicianName, txtClientName].forEach { (txt) in
//            txt?.isUserInteractionEnabled = false
//        }
//        txtClientName.isUserInteractionEnabled = false
        
//        stringTodate(OrignalFormatter: "yyyy-MM-dd hh:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["modified_at"].stringValue)
        
        txtDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "dd-MM-yyyy", strDate: dictData["created_at"].stringValue)
        
        txtvwProblemDescription.text = self.dictData["issue_description"].stringValue
        
        lblProblemDescriptionPlaceHolder.isHidden = self.dictData["issue_description"].stringValue.isEmpty ? false : true
        
        
        let arrayServiceType = [getCommonString(key: "On_Site_key"),
                                getCommonString(key: "Remote_key")]
        
        serviceTypeDD.dataSource = arrayServiceType
        
    }
    
    func registerXib() {
        self.tblTechnitionName.register(UINib(nibName: "TechnitionNameTableCell", bundle: nil), forCellReuseIdentifier: "TechnitionNameTableCell")
    }

}


//MARK: - IBAction

extension AdminReportVC
{
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
       
//        if (txtTechnicianName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_technician_name_key"))
//        }
//        else if (txtClientName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_client_name_key"))
//        }
        if (txtDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        else if (txtDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_select_date_key"))
        }
//        else if (txtvwProblemDescription.text.trimmingCharacters(in: .whitespaces)).isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_problem_key"))
//        }
//        else if (txtServiceType.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_select_service_type_key"))
//        }
        else if (txtServiceDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_select_service_date_key"))
        }
        else if (filterArray.count == 0) {
            makeToast(strMessage: getCommonString(key: "Please_select_technition_name_key"))
        }
        else if (txtTimeSpent.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_time_spent_key"))
        }
//        else if (txtvwServiceDetailsDescription.text.trimmingCharacters(in: .whitespaces)).isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_service_details_key"))
//        }
//        else if (txtvwPartsDescription.text.trimmingCharacters(in: .whitespaces)).isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_parts_key"))
//        }
//        else if (txtvwNotesRequestDescription.text.trimmingCharacters(in: .whitespaces)).isEmpty
//        {
//            makeToast(strMessage: getCommonString(key:"Please_enter_notes/request_key"))
//        }
        else {
            self.addReport()
        }
        
    }
    
    func selectionIndex() {
        
        self.serviceTypeDD.selectionAction = { (index, item) in
            self.txtServiceType.text = item
            self.serviceTypeID = index
            self.view.endEditing(true)
            
            self.serviceTypeDD.hide()
            
        }
    }
    
    @IBAction func btnTechnicionName(_ sender: Any) {
        
        let technicianVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "TechnitionListVC") as! TechnitionListVC
        technicianVC.arrayData = self.arrayTechnicianName
        
        technicianVC.addTechnition = { dicData in
            for i in 0..<self.arrayTechnicianName.count {
                if self.arrayTechnicianName[i]["technician_id"].stringValue == dicData["technician_id"].stringValue {
                    self.arrayTechnicianName[i]["isSelected"].stringValue = "1"
                }
            }
            self.tblTechnitionName.reloadData()
        }
        
        self.tblTechnitionName.reloadData()
        
        technicianVC.modalPresentationStyle = .overCurrentContext
        technicianVC.modalTransitionStyle = .crossDissolve

        self.present(technicianVC, animated: true, completion: nil)
    }

    @objc func btnDeleteAction(_ sender: UIButton ) {
        
        var dicDeleteData = JSON()
        
        if self.filterArray.count > 0 {
            dicDeleteData = self.filterArray[sender.tag]
        }
        
//        dicDeleteData = self.filterArray[sender.tag]
        
        for i in 0..<self.arrayTechnicianName.count {
            if self.arrayTechnicianName[i]["technician_id"].stringValue == dicDeleteData["technician_id"].stringValue {
                self.arrayTechnicianName[i]["isSelected"].stringValue = "0"
            }
        }
        
        self.tblTechnitionName.reloadData()
    }
}

//MARK: Tableview Delegate/Datasource
extension AdminReportVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        self.filterArray = []
        self.arrayTechnicianName.forEach { (data) in
            if data["isSelected"].stringValue == "1" {
                filterArray.append(data)
            }
        }
        
        if filterArray.count > 0 {
            self.btnTechnicianName.isHidden = true
            self.tblTechnitionName.isHidden = false
        }
        else {
            self.tblTechnitionName.isHidden = true
            self.btnTechnicianName.isHidden = false
        }
        
        return filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TechnitionNameTableCell", for: indexPath) as! TechnitionNameTableCell
        
        let dict = self.filterArray[indexPath.row]
        cell.lblTechnitionName.text = dict["technicianname"].stringValue
        
        if dict["isSelected"].stringValue == "1" {
            
            cell.lblTechnitionName.text = dict["technicianname"].stringValue
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        self.filterArray = []
        self.arrayTechnicianName.forEach { (data) in
            if data["isSelected"].stringValue == "0" {
                filterArray.append(data)
            }
        }
        
        let technicianVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "TechnitionListVC") as! TechnitionListVC
        technicianVC.addTechnition = { dicData in
            
            for i in 0..<self.arrayTechnicianName.count {
                if self.arrayTechnicianName[i]["technician_id"].stringValue == dicData["technician_id"].stringValue {
                    self.arrayTechnicianName[i]["isSelected"].stringValue = "1"
                }
            }
            self.tblTechnitionName.reloadData()
        }
        
        technicianVC.arrayData = self.filterArray
        
        technicianVC.modalPresentationStyle = .overCurrentContext
        technicianVC.modalTransitionStyle = .crossDissolve
        
        //if self.filterArray.count != self.arrayTechnicianName.count {
        if self.filterArray.count != 0 {
            DispatchQueue.main.async {
                
                technicianVC.arrayFilter = { array in
                    self.filterArray = array
                    self.tblTechnitionName.reloadData()
                }
//                self.arrayFilter(self.arrayData)
                self.present(technicianVC, animated: false, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.tblHeightConstraint.constant = tableView.contentSize.height
    }
    
}


//MARK: - TextField Delegate

extension AdminReportVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDate
        {
            self.view.endEditing(true)
            
            self.isServiceDate = false
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            obj.isSetMaximumDate = true
            obj.setMaximumDate = Date()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            
            return false
        }
        else if textField == txtServiceDate
        {
            self.view.endEditing(true)
            if (txtDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            {
                makeToast(strMessage: getCommonString(key:"Please_select_date_key"))
                return false
            }
            else
            {
                self.isServiceDate = true
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
                obj.pickerDelegate = self
                obj.controlType = 1
                obj.isSetMaximumDate = true
                obj.setMaximumDate = Date()
                obj.isSetMinimumDate = true
                obj.setMinimumDate = selectedDate
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .coverVertical
                self.present(obj, animated: true, completion: nil)
                
                return false
            }
            
        }
        else if textField == txtServiceType
        {
            self.view.endEditing(true)
            serviceTypeDD.show()
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtTimeSpent
        {
            let maxLength = 3
            let currentString: NSString = txtTimeSpent.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
}

//MARK: - textView Delegate

extension AdminReportVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == txtvwProblemDescription {
            self.lblProblemDescriptionPlaceHolder.isHidden = textView.text == "" ? false : true
        }
        else if textView == txtvwServiceDetailsDescription {
            self.lblServiceDetailsDescriptionPlaceHolder.isHidden = textView.text == "" ? false : true
        }
        else if textView == txtvwPartsDescription {
            self.lblPartsDescriptionPlaceHolder.isHidden = textView.text == "" ? false : true
        }
        else if textView == txtvwNotesRequestDescription {
            self.lblNotesRequestDescriptionPlaceHolder.isHidden = textView.text == "" ? false : true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: - DatePicker

extension AdminReportVC : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date, type: Int) {
        
        if type == 1
        {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd"
//            "dd-MM-yyyy"
            
            let dateFormatterForService = DateFormatter()
            dateFormatterForService.dateFormat = "yyyy-MM-dd hh:mm:ss"
            
            if self.isServiceDate
            {
                serviceDate = dateFormatterForService.string(from: dateValue)
                txtServiceDate.text = dateformatter.string(from: dateValue)
            }
            else
            {
                selectedDate = dateValue
                date = dateFormatterForService.string(from: dateValue)
                txtDate.text = dateformatter.string(from: dateValue)
                txtServiceDate.text = ""
            }
            
        }
        else if type == 0
        {
            
        }
    }
    
}

//MARK: - API Calling

extension AdminReportVC {
    
    func addReport() {
        self.view.endEditing(true)
        
        self.requstDate = stringTodate(OrignalFormatter: "dd-MM-yyyy", YouWantFormatter: "yyyy-MM-dd", strDate: txtDate.text ?? "")
        
        var technitionString = String()
        
        for i in 0..<self.filterArray.count {
            if self.filterArray[i]["isSelected"].stringValue == "1" {
                technitionString = "\(technitionString)"+"\(self.filterArray[i]["technician_id"].stringValue),"
            }
        }
        technitionString.remove(at: technitionString.index(before: technitionString.endIndex))
        
        print("Tech:",technitionString)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(reportURL)\(addReportURL)"
//            makeToast(strMessage: getCommonString(key: "Please_select_technition_name_key"))
            print("URL: \(url)")
            
            var param = ["user_id" : technitionString,
                         "access_token" : getUserDetail("access_token"),
                         "technician_name" : "",
                         "client_name" : txtClientName.text ?? "",
                         "request_date" : self.requstDate,
                         "problem" : txtvwProblemDescription.text ?? "",
                         "service_type" : "\(self.serviceTypeID)",
                         "service_date" : txtServiceDate.text ?? "",
                         "time_spent" : txtTimeSpent.text ?? "",
                         "service_detail" : txtvwServiceDetailsDescription.text ?? ""]
            
            param["parts"] = txtvwPartsDescription.text ?? ""
            param["notes"] = txtvwNotesRequestDescription.text ?? ""
            param["issue_id"] = dictData["issue_id"].stringValue
            param["company_id"] = dictData["company_id"].stringValue
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.navigationController?.popToRootViewController(animated: false)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                         self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func technicianList() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(userURL)\(technicianlist)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "company_id" : self.dictData["company_id"].stringValue,
                         "role" : getUserDetail("role")
            ]
            
            
            print("param :\(param)")
            
//            self.showLoader()
            showActivityIndicatory(view: self.viewTechnicianName)
//            showActivityIndicatory(textField: self.txtTechnicianName)
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                stopActivityIndicatory(view: self.viewTechnicianName)
//                showActivityIndicatory(textField: self.txtTechnicianName)
//                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        var data = json["data"].arrayValue
                        
                        for i in 0..<data.count {
                            var dic = data[i]
                            
                            if (dic["technician_id"].stringValue == getUserDetail("company_id")) {
                                dic["isSelected"] = "1"
                            }
                            else {
                                dic["isSelected"] = "0"
                            }
                            
                            data[i] = dic
                        }
                        self.arrayTechnicianName = data
                        
                        self.tblTechnitionName.reloadData()
                        
//                        self.navigationController?.popToRootViewController(animated: false)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}
