//
//  TechnitionNameTableCell.swift
//  Draco Support
//
//  Created by Khushbu on 22/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TechnitionNameTableCell: UITableViewCell {

    
    //MARK: Outlets
    @IBOutlet var vwBG: CustomView!
    @IBOutlet var lblTechnitionName: UILabel!
    @IBOutlet var btnDelete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTechnitionName].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }

        [vwBG].forEach { (vw) in
            vw?.backgroundColor = UIColor.appthemeRedColor
            vw?.layer.borderColor = UIColor.red.cgColor
            vw?.layer.borderWidth = 1
            vw?.layer.cornerRadius = (vw?.bounds.size.height ?? 20)/2
            vw?.clipsToBounds = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
