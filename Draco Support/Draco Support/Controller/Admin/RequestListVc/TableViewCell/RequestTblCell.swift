//
//  RequestTblCell.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class RequestTblCell: UITableViewCell {

    //MARK: - Outlet
    
    
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblTicketNumberValue: UILabel!
    
    @IBOutlet weak var lblSubmittedDate: UILabel!
    @IBOutlet weak var lblSubmitteddateValue: UILabel!
    
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblCompanyNameValue: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNameValue: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNumberValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    
    @IBOutlet var lblSubmitedTime: UILabel!
    @IBOutlet var lblSumbitedTimeValue: UILabel!
    
    @IBOutlet var vwReceivedBy: UIView!
    @IBOutlet var vwReceivedByHeight: NSLayoutConstraint!
    @IBOutlet var lblReceivedBy: UILabel!
    @IBOutlet var lblReceivedByValue: UILabel!
    
    @IBOutlet weak var btnDelete: CustomButton!
    
    @IBOutlet weak var labInProgressHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnReceivedBy: CustomButton!
    
    //MARK: - Variable
    
    
    //MARK: - Viewlife cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTicketNumber,lblName,lblCompanyName,lblNumber,lblDescription, lblSubmittedDate, lblSubmitedTime, lblReceivedBy].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        lblTicketNumber.text = getCommonString(key: "Ticket_Number_Key")
        lblCompanyName.text = getCommonString(key: "Company_Name_key")
        lblName.text = getCommonString(key: "Submitted_by_key")
        lblNumber.text = getCommonString(key: "Number_key")
        lblDescription.text = getCommonString(key: "Problem_description_key")
        lblSubmittedDate.text = getCommonString(key: "Submitted_Date_key")
        lblSubmitedTime.text = getCommonString(key: "submitted_time_key")
        
        [lblTicketNumberValue,lblNameValue,lblCompanyNameValue,lblNumberValue,lblDescriptionValue, lblSubmitteddateValue, lblSumbitedTimeValue, lblReceivedByValue].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [btnReceivedBy].forEach { (btn) in
            btn?.setThemeButtonUI()
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
