//
//  InProgressVC.swift
//  Draco Support
//
//  Created by Haresh on 23/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class InProgressVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var tblInProgress: UITableView!
    
    //MARK: Variable
    var arrayRequestList : [JSON] = []
    var arrayInProgress : [JSON] = []
    
    var strErrorMessage = ""
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGrayColor
        
        return refreshControl
    }()
    
    //MARK:- Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()

        tblInProgress.register(UINib(nibName: "InProgressTblCell", bundle: nil), forCellReuseIdentifier: "InProgressTblCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestListAPICalling(isShowLoader:)), name: NSNotification.Name(rawValue: "refreshRequestList"), object: nil)
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        isRequestList = true
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Draco_Support_key"), isRightButtonHidden: false)
        setUpBadgeCountAndBarButton()
        requestListAPICalling(isShowLoader: isRequestList)
//        setUpNavigationLeftQRCode()
        NotificationCenter.default.addObserver(self, selector: #selector(setUpBadgeCountAndBarButtonn), name: Notification.Name("notificationGetInProgress"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationGetInProgress"), object: nil)
    }
    
    func setupUI() {

        tblInProgress.dataSource = self
        tblInProgress.delegate   = self
        tblInProgress.addSubview(self.refreshControl)
        tblInProgress.tableFooterView = UIView()
        automaticallyAdjustsScrollViewInsets = false
    }
    
    @objc func setUpBadgeCountAndBarButtonn() {
        setUpBadgeCountAndBarButton()
        requestListAPICalling(isShowLoader: isRequestList)
    }
}


// MARK:- TableView Delegate / Datasource


extension InProgressVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayInProgress.count == 0 {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appthemeRedColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        
        tableView.backgroundView = nil
        return self.arrayInProgress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InProgressTblCell") as! InProgressTblCell
        
        let dict = self.arrayInProgress[indexPath.row]
        
        cell.lblTicketNumberValue.text  = dict["ticketnumber"].stringValue
        cell.lblCompanyNameValue.text = dict["company_name"].stringValue
        cell.lblSubmittedByValue.text = dict["client_name"].stringValue
        cell.lblTechnicianValue.text  = "\(dict["tec_firstname"].stringValue) \(dict["tec_lastname"].stringValue)"
        
        let date = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["created_at"].stringValue)
        
        cell.startTimeValue.text = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "hh:mm a", strDate: dict["modified_at"].stringValue)
        
        cell.lblStartedDateValue.text = "\(date)"
        
        cell.btnDelete.isHidden = true
        if getUserDetail("role") == "1" {
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RequestDetailsvc") as! RequestDetailsvc
        obj.dictIssueData = self.arrayInProgress[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

//MARK:- Other Method

extension InProgressVC {
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        requestListAPICalling(isShowLoader: false)
        refreshControl.endRefreshing()
    }
    
    //MARK: - Delete Action
    @objc func btnDeleteAction(sender: UIButton) {
        let dic = self.arrayRequestList[sender.tag]
        print("dicdata:\(dic)")
        
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_you_want_to_delete_this_data?"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.deleteRowValue(issueId: dic["issue_id"].stringValue)
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        //        self.deleteRowValue(issueId: dic["issue_id"].stringValue)
    }
}

//MARK:- API Calling

extension InProgressVC {
    
    @objc func requestListAPICalling(isShowLoader:Bool)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(inProgress)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id")
                
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.showLoader()
            }
            
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader
                {
                    self.hideLoader()
                }
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayRequestList = []
                        self.arrayRequestList = data
                        
                        //filter self.arrayRequestList["status"] value
                        self.arrayInProgress = []
                        self.arrayInProgress = data.filter{$0["status"] == "in_progress"}
                        
                        print("ArryIn Progress:-- \(self.arrayInProgress)")
                        print("ArrayInProgress count: \(self.arrayInProgress.count)")
                        
//                        arrayInProgress = self.arrayRequestList.filter{$0.stringValue == self.arrayRequestList["status"]}
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
//                        self.arrayRequestList = []
                        self.arrayInProgress = []
                        self.strErrorMessage = json["msg"].stringValue
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblInProgress.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    @objc func deleteRowValue(issueId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicURLForClient)\(issueDetailURL)\(DeleteDataRequest)"
            
            print("URL: \(url)")
            
            let param = ["issue_id" : issueId,
                         "user_id" : getUserDetail("id")
                
            ]
            
            print("param :\(param)")
            
            //            if isShowLoader
            //            {
            //                self.showLoader()
            //            }
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                //                if isShowLoader
                //                {
                //                    self.hideLoader()
                //                }
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        //                        let data = json["data"].arrayValue
                        
                        self.requestListAPICalling(isShowLoader: true)
                        
                        //                        self.arrayRequestList = []
                        //                        self.arrayRequestList = data
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.strErrorMessage = json["msg"].stringValue
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    makeToast(strMessage: "Data delete successfully!!!")
                    //                    self.tblRequestList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
}
