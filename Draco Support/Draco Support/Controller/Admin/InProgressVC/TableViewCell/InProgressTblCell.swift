//
//  InProgressTblCell.swift
//  Draco Support
//
//  Created by Haresh on 23/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class InProgressTblCell: UITableViewCell {

    //Mark:- Outlets
    
    @IBOutlet weak var lblTicketNumber      : UILabel!
    @IBOutlet weak var lblTicketNumberValue : UILabel!
    
    @IBOutlet weak var lblCompanyName       : UILabel!
    @IBOutlet weak var lblCompanyNameValue  : UILabel!
    
    @IBOutlet weak var lblSubmittedBy       : UILabel!
    @IBOutlet weak var lblSubmittedByValue  : UILabel!
    
    @IBOutlet weak var lblTechnician        : UILabel!
    @IBOutlet weak var lblTechnicianValue   : UILabel!
    
    @IBOutlet weak var lblStartedDate       : UILabel!
    @IBOutlet weak var lblStartedDateValue  : UILabel!
    
    @IBOutlet var startTime: UILabel!
    @IBOutlet var startTimeValue: UILabel!
    
    
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        [lblTicketNumber, lblCompanyName, lblSubmittedBy, lblTechnician, lblStartedDate, startTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        lblTicketNumber.text = getCommonString(key: "Ticket_Number_Key")
        lblCompanyName.text  = getCommonString(key: "Company_Name_key")
        lblSubmittedBy.text  = getCommonString(key: "Submitted_by_key")
        lblTechnician.text   = getCommonString(key: "Technician_Name_key")
        lblStartedDate.text  = getCommonString(key: "Started_date_key")
        startTime.text = getCommonString(key: "Start_Time_key")
        
        [lblTicketNumberValue, lblCompanyNameValue, lblSubmittedByValue, lblTechnicianValue, lblStartedDateValue, startTimeValue].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
    }
    
}
