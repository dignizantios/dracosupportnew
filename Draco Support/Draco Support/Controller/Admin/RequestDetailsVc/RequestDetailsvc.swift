//
//  RequestDetailsvc.swift
//  Draco Support
//
//  Created by YASH on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import NVActivityIndicatorView
import SDWebImage
import GoogleMaps

class RequestDetailsvc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var collectionImgView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNameValue: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNumberValue: UILabel!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblCompanyNameValue: UILabel!
    
    @IBOutlet weak var lblEquipmentName: UILabel!
    @IBOutlet weak var lblEquipmentNameValue: UILabel!
    
    @IBOutlet weak var lblManufactor: UILabel!
    @IBOutlet weak var lblManufactorValue: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblLocationValue: UILabel!
    
    @IBOutlet var vwProblemDescription: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    @IBOutlet var vwDescriptionData: UIView!
    @IBOutlet var vwDescription: CustomView!
    
    @IBOutlet var vwDescriprionHeight: NSLayoutConstraint!
    @IBOutlet var lblEnterHere: UILabel!
    @IBOutlet var txtVwDescription: UITextView!
    @IBOutlet var btnDescriptionSave: CustomButton!
    
    @IBOutlet weak var vwMap: GMSMapView!
    @IBOutlet weak var heightConstraintVWMap: NSLayoutConstraint!
    
    @IBOutlet weak var btnNeedAssist: CustomButton!
    @IBOutlet weak var btnFinish: CustomButton!
    
    @IBOutlet weak var btnStart: CustomButton!
    
    @IBOutlet weak var vwStart: UIView!
    @IBOutlet weak var vwNeedAssist: UIView!
    @IBOutlet weak var vwFinish: UIView!
    
    
    //MARK: - Variable
    
    var dictIssueData = JSON()
    
    var arrayImages : [JSON] = []
    
    var locationManager = CLLocationManager()
    var isStartRequest : () -> Void = {}
    
    
    
    //MARK: - Viewlife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
        
        self.setupUI()
        self.showMap()
        
        collectionImgView.register(UINib(nibName: "HeaderImagesCollectionCell", bundle: nil), forCellWithReuseIdentifier:"HeaderImagesCollectionCell")
        print("dictIssueData: ---- \(dictIssueData)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
    }
    
    
}

//MARK: - SetupUI

extension RequestDetailsvc {
    
    func setupUI() {
        
        hidekeyBoardWhenTappedAround()
        
        pageControl.currentPageIndicatorTintColor = UIColor.appthemeRedColor
        pageControl.tintColor = UIColor.white
        
        [lblName,lblNumber,lblCompanyName,lblEquipmentName,lblManufactor,lblLocation,lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [lblNameValue,lblNumberValue,lblCompanyNameValue,lblEquipmentNameValue,lblManufactorValue,lblLocationValue,lblDescriptionValue, lblEnterHere].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [vwDescription].forEach { (vw) in
            vw?.cornerRadius = 7
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        [txtVwDescription].forEach { (txtVw) in
            txtVw?.font = themeFont(size: 16, fontname: .regular)
            txtVw?.textColor = .black
            txtVw?.tintColor = UIColor.appthemeRedColor
        }
        
        [btnDescriptionSave].forEach { (btn) in
            btn?.setThemeButtonUI()
        }
        
        //  Description_key Location_Key
        
        lblName.text = getCommonString(key: "Name_:_key")
        lblNumber.text = getCommonString(key: "Number_key")
        lblCompanyName.text = getCommonString(key: "CompanyName_Key")
        lblEquipmentName.text = getCommonString(key: "Equipment_Name_Key")
        lblManufactor.text = getCommonString(key: "Manufacturer_Key")
        lblLocation.text = getCommonString(key: "Location_Key")
        lblDescription.text = getCommonString(key: "Problem_description_key")
        
        btnNeedAssist.setThemeButtonUI()
        btnNeedAssist.setTitle(getCommonString(key: "Return_to_Queue_key"), for: .normal)
//        btnNeedAssist.setTitle(getCommonString(key: "Need_Assist_key"), for: .normal)
       
        btnStart.setThemeButtonUI()
        btnStart.setTitle(getCommonString(key: "Start_key"), for: .normal)
        
        btnFinish.setThemeButtonUI()
        btnFinish.backgroundColor = UIColor.white
        btnFinish.setTitle(getCommonString(key: "Finish_key"), for: .normal)
        btnFinish.setTitleColor(UIColor.appThemeDarkGrayColor, for: .normal)
        
        //show diffrent button diffrent admin
        //SetData
        
//        if self.dictIssueData["request_flag"].stringValue == "show"
//        {
//            vwStart.isHidden = false
//
//            [vwNeedAssist,vwFinish].forEach { (vw) in
//                vw?.isHidden = true
//            }
//        }
//        else
//        {
//            vwStart.isHidden = true
//
//            [vwNeedAssist,vwFinish].forEach { (vw) in
//                vw?.isHidden = false
//            }
//        }
        
        print("dictIssueData: ---- \(dictIssueData)")
        
        if (self.dictIssueData["view_need_status"].stringValue == "All hide") {
            [vwStart, vwNeedAssist, vwFinish].forEach { (vw) in
                vw?.isHidden = true
            }
            
        }
        else if (self.dictIssueData["view_need_status"].stringValue == "start finish") {
           
            vwStart.isHidden = false
            [vwNeedAssist, vwFinish].forEach { (vw) in
                vw?.isHidden = true
            }
            
            if (self.dictIssueData["status"].stringValue == "in_progress") {
                self.vwStart.isHidden = true
                [self.vwFinish].forEach{ (vw) in
                    vw?.isHidden = false
                }
                
            }

        } else if (self.dictIssueData["view_need_status"].stringValue == "new request"){
            vwStart.isHidden = false
            [vwNeedAssist, vwFinish].forEach { (vw) in
                vw?.isHidden = true
            }
            
            if (self.dictIssueData["status"].stringValue == "in_progress") {
                self.vwStart.isHidden = true
                [self.vwFinish, self.vwNeedAssist].forEach{ (vw) in
                    vw?.isHidden = false
                }
                if (self.dictIssueData["need_assist_status"].stringValue == "1") {
                    self.vwFinish.isHidden = false
                    
                    [self.vwNeedAssist,self.vwStart].forEach { (vw) in
                        vw?.isHidden = true
                    }
                }

            }
            
        }
        
        if self.vwNeedAssist.isHidden == true && self.vwFinish.isHidden == true {
            self.lblDescriptionValue.isHidden = false
            self.vwDescriptionData.isHidden = true
//            self.vwDescriprionHeight.constant = 0
        }
        else {
            self.lblDescriptionValue.isHidden = true
//            self.vwDescriprionHeight.constant = 135
            self.vwDescriptionData.isHidden = false
        }
        
        
        lblNameValue.text = self.dictIssueData["client_name"].stringValue
        lblNumberValue.text = self.dictIssueData["contact_no"].stringValue
        lblDescriptionValue.text = self.dictIssueData["issue_description"].stringValue
        lblEquipmentNameValue.text = self.dictIssueData["computer_name"].stringValue
        lblManufactorValue.text = self.dictIssueData["manufacturer"].stringValue
        lblLocationValue.text = self.dictIssueData["location"].string
        lblCompanyNameValue.text = self.dictIssueData["company_name"].stringValue
        
        txtVwDescription.text = self.dictIssueData["issue_description"].stringValue
        lblEnterHere.isHidden = self.dictIssueData["issue_description"].stringValue.isEmpty ? false : true
        
        self.arrayImages = self.dictIssueData["picture"].arrayValue
        
        
        if self.txtVwDescription.text != dictIssueData["issue_description"].stringValue {
            btnDescriptionSave.isUserInteractionEnabled = true
            self.btnDescriptionSave.setThemeButtonUI()
        }
        else {
            btnDescriptionSave.isUserInteractionEnabled = false
            self.btnDescriptionSave.backgroundColor = UIColor.appThemeDarkGrayColor
            self.btnDescriptionSave.layer.borderWidth = 0
        }
        
        self.collectionImgView.reloadData()
        
    }
    
}

//MARK: - IBAction method

extension RequestDetailsvc {
    
    @IBAction func btnDescriptionSaveAction(_ sender: Any) {
        
        if self.txtVwDescription.text != dictIssueData["issue_description"].stringValue {
            problemDescriptionChange()
        }
    }
    
    @IBAction func btnNeedAssistTapped(_ sender: UIButton) {
        needAssistAPICalling()
    }
    
    
    @IBAction func btnFinishTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminReportVC") as! AdminReportVC
        obj.dictData = self.dictIssueData
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnStartTapped(_ sender: UIButton) {
        
//        self.isStartRequest()
        startIssueAPICalling()
    }
}


//MARK: - ScrollDelegate method

extension RequestDetailsvc : UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        print("pageControl.currentPage:\(pageControl.currentPage)")
    }
    
}


//MARK: TextView Delegate
extension RequestDetailsvc : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if txtVwDescription.text.trimmingCharacters(in: .whitespaces).isEmpty {
            self.lblEnterHere.isHidden = false
        }
        else {
            self.lblEnterHere.isHidden = true
        }
        
        if self.txtVwDescription.text != dictIssueData["issue_description"].stringValue {
            btnDescriptionSave.isUserInteractionEnabled = true
            self.btnDescriptionSave.setThemeButtonUI()
        }
        else {
            btnDescriptionSave.isUserInteractionEnabled = false
            self.btnDescriptionSave.backgroundColor = UIColor.appThemeDarkGrayColor
            self.btnDescriptionSave.layer.borderWidth = 0
        }
    }
    
    // hides text views
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            txtVwDescription.resignFirstResponder()
            return false
        }
        return true
    }
    
}


//MARK:- Map Integration
extension RequestDetailsvc : CLLocationManagerDelegate {
    
    func showMap() {
        
        print("self.vwMap.frame.width: \(self.vwMap.frame.width)")
        self.heightConstraintVWMap.constant = self.vwMap.frame.width
        
        // Google Map View Setup
        let lat  = Double(self.dictIssueData["lattitude"].stringValue) ?? Double("0.0")
        let long = Double(self.dictIssueData["longitude"].stringValue) ?? Double("0.0")
        
        
        let camera = GMSCameraPosition.camera(withLatitude: lat!,  longitude: long!, zoom: 12.0)
        self.vwMap.camera = camera
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lat!, long!)
        marker.map = self.vwMap
        self.vwMap.isUserInteractionEnabled = false
        
    }
    
//    //setup google map
//    func setMap() {
//        self.vwMap.layer.borderColor = UIColor.appthemeRedColor.cgColor
//        self.vwMap.layer.borderWidth = 1
//        self.vwMap.layer.cornerRadius = 10
//
//        self.vwMap.isUserInteractionEnabled = false
//        //        self.vwMap.settings.setAllGesturesEnabled(true)
//        //        self.vwMap.settings.allowScrollGesturesDuringRotateOrZoom = false
//
//        initializeTheLocationManager()
//        self.vwMap.settings.myLocationButton = true
//        self.vwMap.isMyLocationEnabled = true
//    }
//
//    func initializeTheLocationManager() {
//        locationManager.delegate = self
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.startUpdatingLocation()
//    }
//
//    //Get device location
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        let location = locationManager.location?.coordinate
//
//
//
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker(position: location!)
//        marker.title = "My Location"
//        //        marker.snippet = "Australia"
//        marker.map = self.vwMap
//
//        cameraMoveToLocation(toLocation: location)
//    }
//
//    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
//        if toLocation != nil {
//            self.vwMap.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 10.0)
//        }
//    }
    
}


//MARK: - CollectionView Delegate datasource
extension RequestDetailsvc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.arrayImages.count == 0
        {
            let img = UIImageView()
            img.image = UIImage(named: "ic_plas_holder")
            img.center = collectionView.center
            collectionView.backgroundView = img
            
            self.pageControl.isHidden = true
            
            return 0
        }
        
        self.pageControl.isHidden = false
        self.pageControl.numberOfPages = arrayImages.count
        return arrayImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionImgView.dequeueReusableCell(withReuseIdentifier: "HeaderImagesCollectionCell", for: indexPath) as! HeaderImagesCollectionCell

        // cell.imgHeader.image = UIImage(named: "parts")
        
        let dict = self.arrayImages[indexPath.row]
        
        cell.imgHeader.sd_setShowActivityIndicatorView(true)
        cell.imgHeader.sd_setIndicatorStyle(.white)
        
        cell.imgHeader.sd_setImage(with: dict["picture"].url, placeholderImage: UIImage(named:"ic_plas_holder"), options: .lowPriority, completed: nil)
//        cell.imgHeader.sd_setImage(with: dict["picture"].url, placeholderImage: UIImage(named:"ic_spalsh_holder_add_image"), options: .lowPriority, completed: nil)
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionImgView.frame.width, height: self.collectionImgView.frame.height)
    }
    
}

//MARK: - API calling

extension RequestDetailsvc
{
    
    func startIssueAPICalling()
    {
        self.view.endEditing(true)
        
        if self.dictIssueData["view_received_status"].stringValue == "Receive" {
            self.issueReceivedValue(isShowLoader: true, issueId: self.dictIssueData["issue_id"].stringValue, issueSenderId: self.dictIssueData["client_user_id"].stringValue, companyId: self.dictIssueData["company_id"].stringValue)
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(startIssueURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id"),
                         "issue_id" : self.dictIssueData["issue_id"].stringValue
                
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
//                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.vwStart.isHidden = true
                        
                        [self.vwNeedAssist,self.vwFinish].forEach { (vw) in
                            vw?.isHidden = false
                        }
                        
                        if (self.dictIssueData["need_assist_status"].stringValue == "1") {
                            self.vwFinish.isHidden = false

                            [self.vwNeedAssist,self.vwStart].forEach { (vw) in
                                vw?.isHidden = true
                            }
                        }
                        makeToast(strMessage: json["msg"].stringValue)
                        self.lblDescriptionValue.isHidden = true
                        self.vwDescriptionData.isHidden = false
                    }
                    else if json["msg"].stringValue == GlobalVariables.alreadyRegister
                    {
                        
                        if (self.dictIssueData["status"].stringValue == "in_progress") {
                            self.vwFinish.isHidden = false
                            [self.vwStart, self.vwNeedAssist].forEach{ (vw) in
                                vw?.isHidden = true
                            }
                        }
                        
//                        if (self.dictIssueData["status"].stringValue == "in_progress") {
//                            self.vwFinish.isHidden = false
//                            [self.vwStart, self.vwNeedAssist].forEach{ (vw) in
//                                vw?.isHidden = true
//                            }
//                        }
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func needAssistAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(needAssistURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id"),
                         "issue_id" : self.dictIssueData["issue_id"].stringValue
                
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
//                        self.navigationController?.popViewController(animated: false)
                        makeToast(strMessage: json["msg"].stringValue)
                        appdelgate.objCustomTabBar.selectedIndex = 0
                        self.navigationController?.popViewController(animated: false)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func problemDescriptionChange() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicURLForClient)\(issueDetailURL)\(updateProblemdesc)"
            
            print("URL: \(url)")
            
            let param = ["problem_description" : txtVwDescription.text ?? self.dictIssueData["issue_description"].stringValue,
                         "user_id" : getUserDetail("id"),
                         "issue_id" : self.dictIssueData["issue_id"].stringValue
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let msg = self.txtVwDescription.text ?? self.dictIssueData["issue_description"].stringValue
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.dictIssueData["issue_description"].stringValue = msg
                        
                        self.btnDescriptionSave.backgroundColor = UIColor.appThemeDarkGrayColor
                        self.btnDescriptionSave.layer.borderWidth = 0
                        self.btnDescriptionSave.isUserInteractionEnabled = false
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    @objc func issueReceivedValue(isShowLoader:Bool, issueId: String, issueSenderId: String, companyId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicURLForClient)\(issueDetailURL)\(issueReceived)"
            
            print("URL: \(url)")
            
            let param = ["issue_id" : "\(issueId)",
                "user_id" : getUserDetail("id"),
                "issue_sender_id":issueSenderId,
                "company_id":companyId
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.showLoader()
            }
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader
                {
                    self.hideLoader()
                }
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        self.issueReceivedDataUpdate(updateData: json["data"], positionId: positionId)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
//                        self.strErrorMessage = json["msg"].stringValue
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    //                    makeToast(strMessage: "Data delete successfully!!!")
                    //                    self.tblRequestList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
}


