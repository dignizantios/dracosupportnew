//
//  HeaderImagesCollectionCell.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class HeaderImagesCollectionCell: UICollectionViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var imgHeader: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
