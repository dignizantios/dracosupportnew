//
//  HistoryVc.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class HistoryVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var btnFilterBy: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var txtSearchdata: CustomTextField!
    
    
    @IBOutlet weak var vwTop: UIView!
    
    @IBOutlet weak var tblHistoryList: UITableView!
    
    //MARK: - Variable
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGrayColor
        
        return refreshControl
    }()
    
    
    var arrayHistoryList : [JSON] = []
    var strErrorMessage = ""
    
    var arrayFilter : [JSON] = []

    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblHistoryList.register(UINib(nibName: "HistoryTblCell", bundle: nil), forCellReuseIdentifier: "HistoryTblCell")
        
        tblHistoryList.tableFooterView = UIView()
        
       setupUI()
        
        txtSearchdata.delegate = self
        txtSearchdata.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
                
        self.isAccessibilityElement = true
        self.vwSearch.isHidden = true
        self.btnFilterBy.isHidden = false
        
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "History_key"), isRightButtonHidden: false)
        setUpBadgeCountAndBarButton()
//        setUpNavigationLeftQRCode()
        historyAPICalling(isShowLoader: true)
        self.txtSearchdata.resignFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(setUpBadgeCountAndBarButton), name: Notification.Name("notificationGetHistory"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationGetHistory"), object: nil)
    }
    
    func setupUI()
    {
        
        addDoneButtonOnKeyboard(textfield: txtSearchdata)
        
        self.txtSearchdata.setThemeTextFieldUI()
        self.txtSearchdata.placeholder = getCommonString(key: "Search_Key")
        
        btnFilterBy.setThemeButtonUI()
        btnFilterBy.setTitle(getCommonString(key: "Filter_By_Key"), for: .normal)
        btnFilterBy.layer.cornerRadius = 13
        
        
        tblHistoryList.delegate = self
        tblHistoryList.dataSource = self
        self.tblHistoryList.addSubview(self.refreshControl)
        automaticallyAdjustsScrollViewInsets = false
    }
    
    @objc func setUpBadgeCountAndBarButtonn() {
        
        setUpBadgeCountAndBarButton()
        historyAPICalling(isShowLoader: true)
    }
    
    //MARK:- Button action
    
    @IBAction func btnFilterByAction(_ sender: UIButton) {
        
        let filterVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        filterVC.delegare = self
        filterVC.modalPresentationStyle = .overCurrentContext
        filterVC.modalTransitionStyle   = .crossDissolve
        
        present(filterVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if self.isAccessibilityElement == true {
            self.view.endEditing(true)
            self.isAccessibilityElement = false
            self.btnFilterBy.isHidden = true
            self.vwSearch.isHidden  = false
            self.txtSearchdata.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
            self.isAccessibilityElement = true
            self.btnFilterBy.isHidden = false
            self.vwSearch.isHidden  = true
            self.txtSearchdata.resignFirstResponder()
        }
        
    }
    
}

//MARK: - UITableview Delegate
extension HistoryVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("arrayHistoryList.count :-  \(arrayHistoryList.count)")
         if arrayHistoryList.count == 0
         {
             let lbl = UILabel()
             lbl.text = strErrorMessage
             lbl.textAlignment = NSTextAlignment.center
             lbl.textColor = UIColor.appthemeRedColor
             lbl.center = tableView.center
             tableView.backgroundView = lbl
            
             return 0
         }
         
         tableView.backgroundView = nil
         return arrayHistoryList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTblCell") as! HistoryTblCell
        
        let dict = self.arrayHistoryList[indexPath.row]
        
        cell.lblTicketNumberValue.text = dict["ticketnumber"].stringValue
        cell.lblClientNameValue.text = dict["client_name"].stringValue
        cell.lblEmailValue.text = getUserDetail("email")
        cell.lblTechNameValue.text = dict["technician_name"].stringValue
//        cell.lblCompanyNameValue.text = getUserDetail("company_name")
        cell.lblCompanyNameValue.text = dict["company_name"].stringValue
        cell.lblSubmitedByValue.text = dict["client_name"].stringValue
        
        let submittedDate = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["request_date"].stringValue)
        cell.lblSubmittedDateValue.text = "\(submittedDate)"
        
        let date = stringTodate(OrignalFormatter: "yyyy-MM-dd HH:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["service_date"].stringValue)
        
        let timeSpent = dict["time_spent"].intValue
        
        let min = timeSpent % 60
        let hrs = timeSpent / 60
        
        var value = ""
        
        if min == 0
        {
            value = "\(hrs) hrs"
        }
        else if hrs == 0
        {
            value = "\(min) min"
        }
        else
        {
            value = "\(hrs) hrs \(min) min"
        }
        cell.lblDateTimeValue.text = "\(date)"
//        cell.lblDateTimeValue.text = "\(date)   \(value)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        self.view.endEditing(true)
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminReportDetailsVC") as! AdminReportDetailsVC
        obj.dictReportData = self.arrayHistoryList[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

//MARK:- FilterData Delegate

extension HistoryVc: FilterDataDelegate  {
    
    func filterData(technician_id: String, company_id: String, start_date: String, end_date: String) {
        
        print("technician id:\(technician_id) \n company ID:\(company_id)\n start_date:\(start_date)\n end date:\(end_date)")
        
//        let topIndex = IndexPath(row: 0, section: 0)
//        self.tblHistoryList.scrollToRow(at: topIndex, at: .top, animated: true)
        
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(reportURL)\(historyListURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id"),
                         "technician_id":"\(technician_id)",
                "company_id":"\(company_id)" ,
                "start_date": "\(start_date)",
                "end_date":"\(end_date)"
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayHistoryList = []
                        self.arrayHistoryList = data
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.arrayHistoryList = []
                        self.strErrorMessage = json["msg"].stringValue
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblHistoryList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
}

//MARK: - Other method

extension HistoryVc
{
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        historyAPICalling(isShowLoader: false)
        refreshControl.endRefreshing()
        
    }
}

//MARK:- TextField Delegate

extension HistoryVc : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        guard let text = textField.text else { return }
        if text == "" {
            self.arrayHistoryList = self.arrayFilter
        }
        else {
            
            self.arrayHistoryList = self.arrayFilter.filter({ (dict) -> Bool in
                if dict["ticketnumber"].stringValue.contains(text) {
                    return true
                }
                return false
            })
            
            print("filter Array \(self.arrayHistoryList)")
        }
        
        self.tblHistoryList.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
    
}


//MARK: - API calling

extension HistoryVc
{
    
    func historyAPICalling(isShowLoader:Bool)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(reportURL)\(historyListURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id")
                
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                self.showLoader()
            }
            
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader
                {
                    self.hideLoader()
                }
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayHistoryList = []
                        self.arrayHistoryList = data
                        
                        self.arrayFilter = self.arrayHistoryList
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                         self.logoutAPICalling()
                    }
                    else
                    {
                        self.strErrorMessage = json["msg"].stringValue
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblHistoryList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
}
