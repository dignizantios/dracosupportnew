//
//  SuccessPopVc.swift
//  Draco Support
//
//  Created by Haresh on 09/04/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SuccessPopVc: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var btnDone: UIButton!
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        btnDone.setThemeButtonUI()
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Draco_Support_key"), isRightButtonHidden: true)
        
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }

        
    }
    
}
