//
//  SubmitDetailsVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import TOCropViewController
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import DropDown
import GoogleMaps

enum checkParentControllerOfSubmitDetails
{
    case withoutScan
    case withScanOrNumber
}


class SubmitDetailsVc: UIViewController, CLLocationManagerDelegate {

    //MARK: - Outlet
    
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    @IBOutlet weak var txtvwIssueDescription: CustomTextview!
    @IBOutlet weak var lblIssueDescriptionPlaceHolder: UILabel!
    
    
    @IBOutlet weak var vwCompanyName: CustomView!
    @IBOutlet weak var txtCompnyName: CustomTextField!
    
    
    @IBOutlet weak var vwUserName: CustomView!
    @IBOutlet weak var vwPhoneNumber: CustomView!
    @IBOutlet weak var vwIssueDescription: CustomView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var lblAddImage: UILabel!
    @IBOutlet weak var collectionImage: UICollectionView!
    
    @IBOutlet weak var vwEquipmetnName: CustomView!
    @IBOutlet weak var txtEquipmentName: CustomTextField!
    
    @IBOutlet weak var vwManufacture: CustomView!
    @IBOutlet weak var txtvwManufacture: CustomTextview!
    @IBOutlet weak var lblPlaceholderManufacture: UILabel!
    
    
    @IBOutlet weak var vwLocationequipment: CustomView!
    @IBOutlet weak var txtvwLocationOfEquipment: CustomTextview!
    
    @IBOutlet weak var lblLocationOfEquipment: UILabel!
    
    @IBOutlet var lblCompany: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPhoneNumber: UILabel!
    @IBOutlet var lblEquipmentModel: UILabel!
    @IBOutlet var lblEquipmentAndModel: UILabel!
    @IBOutlet var lblLocationOfTheEquipment: UILabel!
    @IBOutlet var lblProblemDescription: UILabel!
    @IBOutlet var lblTechnicianName: UILabel!
    
    @IBOutlet var lblAlternamePhoneNumber: UILabel!
    @IBOutlet var vwAlternatePhoneNumber: CustomView!
    @IBOutlet var txtAlternatePhoneNumber: CustomTextField!
    
    @IBOutlet var lblExt: UILabel!
    @IBOutlet var vwExt: CustomView!
    @IBOutlet var txtExt: CustomTextField!
    
    
    //MARK: - Variable
    
    var arrayImage = [UIImage]()
    
     private var customImagePicker = CustomImagePicker()
    
    var scanData = JSON()
    
    var selectedController = checkParentControllerOfSubmitDetails.withScanOrNumber
    
    var companyNameDD = DropDown()
    var strCompnayID = ""
    var arrayCompanyName : [JSON] = []
    var arrayCompanyNameString = [String]()
    
    //Map Data:
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var lat = Double()
    var long = Double()
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
//        getCompanyOrganizationList()
        locManager.delegate = self
        locManager.requestWhenInUseAuthorization()
        
        appdelgate.objCurrentLocationDelegate = self
        appdelgate.setUpQuickLocationUpdate()
        
        getCurrentLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getCurrentLocation()
        
        isScanScreenForIssue = true

        self.navigationController?.navigationBar.isHidden = false
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "Draco Support")
        
        collectionImage.register(UINib(nibName: "MultiSelectImgCollectionCell", bundle: nil), forCellWithReuseIdentifier:"MultiSelectImgCollectionCell")
    }

    override func viewWillDisappear(_ animated: Bool) {
       isScanScreenForIssue = false
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.companyNameDD, sender: self.txtCompnyName)
        selectionIndex()
    }
}

//MARK: - SetupUI
extension SubmitDetailsVc
{
    
    func setupUI()
    {
        
        [lblCompany, lblName, lblPhoneNumber, lblEquipmentModel, lblEquipmentAndModel, lblLocationOfTheEquipment, lblProblemDescription, lblAlternamePhoneNumber, lblExt].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        lblCompany.text = getCommonString(key: "Company/Organization_key")
        lblName.text = getCommonString(key: "Name_key")
        lblPhoneNumber.text = getCommonString(key: "Phone_Number_key")
        lblEquipmentModel.text = getCommonString(key: "Equipment_Name_place_key")
        lblEquipmentAndModel.text = getCommonString(key: "Equipment_and_Model_key")
        lblLocationOfTheEquipment.text = getCommonString(key: "Location_of_the_Equipment_place_key")
        lblProblemDescription.text = getCommonString(key: "Problem_Description_key")
        lblAlternamePhoneNumber.text = getCommonString(key: "Altername_phone_number_key")
        lblExt.text = getCommonString(key: "Ext_key")
        
        txtAlternatePhoneNumber.placeholder = getCommonString(key: "Altername_phone_number_key")
        txtExt.placeholder = getCommonString(key: "Ext_key")
        
        [txtAlternatePhoneNumber, txtExt].forEach { (txt) in
            addDoneButtonOnKeyboard(textfield: txt)
        }
        
        self.txtEquipmentName.text = self.scanData["computer_name"].stringValue
        self.txtvwLocationOfEquipment.text = self.scanData["location"].stringValue
        self.txtvwManufacture.text = self.scanData["manufacturer"].stringValue
        self.lblPlaceholderManufacture.isHidden = true
        self.lblLocationOfEquipment.isHidden = true
        
        txtCompnyName.text = getUserDetail("company_name")
        self.strCompnayID = getUserDetail("company_id")
        txtUserName.text = "\(getUserDetail("firstname")) \(getUserDetail("lastname"))"
        txtPhoneNumber.text = getUserDetail("mobile")
        
        [txtUserName,txtPhoneNumber,txtCompnyName,txtEquipmentName, txtAlternatePhoneNumber, txtExt].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 55
            txt?.setThemeTextFieldUI()
            
        }
        
        txtExt.leftPaddingView = 15
        
        [txtvwIssueDescription,txtvwManufacture,txtvwLocationOfEquipment].forEach { (txtvw) in
            txtvw?.delegate = self
            txtvw?.tintColor = UIColor.appthemeRedColor
            txtvw?.font = themeFont(size: 16, fontname: .regular)
        }
        
        txtEquipmentName.leftPaddingView = 20
        txtEquipmentName.placeholder = getCommonString(key: "Equipment_Name_place_key")
        [vwUserName,vwPhoneNumber,vwIssueDescription,vwCompanyName,vwEquipmetnName,vwManufacture,vwLocationequipment, vwAlternatePhoneNumber, vwExt].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        vwIssueDescription.cornerRadius = 5.0
        vwManufacture.cornerRadius = 5.0
        vwLocationequipment.cornerRadius = 5.0
        
        txtCompnyName.placeholder = getCommonString(key: "Company/Organization_key")
        txtUserName.placeholder = getCommonString(key: "Name_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_Number_key")
        lblIssueDescriptionPlaceHolder.text = getCommonString(key: "Problem_Description_key")
        lblPlaceholderManufacture.text = getCommonString(key: "Manufacture/Model_place_key")
        lblLocationOfEquipment.text = getCommonString(key: "Location_of_the_Equipment_place_key")
        [lblIssueDescriptionPlaceHolder,lblPlaceholderManufacture,lblLocationOfEquipment].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        
        lblAddImage.textColor = UIColor.black
        lblAddImage.font = themeFont(size: 16, fontname: .regular)
        lblAddImage.text = getCommonString(key: "Add_Image_key")
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        
    }
    
}


extension SubmitDetailsVc
{
    //MARK: - IBAction method
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        if (txtCompnyName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_select_company/Organizatino_key"))
        }
        else if (txtUserName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_name_key"))
        }
        else if (txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_phone_number_key"))
        }
        else if (txtPhoneNumber.text?.isNumeric == false) || (txtPhoneNumber.text!.count < GlobalVariables.phoneNumberLimit)
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_phone_number_key"))
        }
        else if (txtEquipmentName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_euipment_name_key"))
        }
        else if (txtvwIssueDescription.text.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_issue_description_key"))
        }
        else if (txtvwManufacture.text.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_manufacture/Model_key"))
        }
        else if (txtvwLocationOfEquipment.text.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_location_of_equipment_key"))
        }
//        else if arrayImage.count == 0
//        {
//            makeToast(strMessage: getCommonString(key: "Please_add_image_key"))
//        }
        else
        {
            submitDetailsAPICalling()
        }
 
    }
    
    //MARK: Map current location
    func getCurrentLocation() {
        locManager.requestWhenInUseAuthorization()
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locManager.location
            
        }
//        print("Location long: ----  \(currentLocation.coordinate.longitude)")
//        print("Location lat: ----  \(currentLocation.coordinate.latitude)")
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    //MARK: - Other method
    
    @objc func imgSelection(_ sender : UIButton)
    {
        selectImages()
    }
    
    @objc func btnRemoveImage(_ sender : UIButton)
    {
        self.arrayImage.remove(at: sender.tag)
        self.collectionImage.reloadData()
    }
    
    func selectImages()
    {
        customImagePicker.typeOfPicker = .onlyPhoto
        
        customImagePicker.showImagePicker(fromViewController: self,
                                          navigationColor: UIColor.appthemeRedColor,
                                          imagePicked: { (response) in
                                            
                                            print("response:\(response)")
                                            
                                            let theImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
//
                                            self.openCropVC(Image: theImage)
                                            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    
    //MARK: - DropDown Selection method
    
    
    func selectionIndex()
    {
        
        self.companyNameDD.selectionAction = { (index, item) in
            self.txtCompnyName.text = item
            self.view.endEditing(true)
            self.strCompnayID = (self.arrayCompanyName[index]["id"]).stringValue
            
//            self.companyNameDD.hide()
            
        }
    }

}

//MARK: - TextField Delegate

extension SubmitDetailsVc: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtPhoneNumber
        {
            let maxLength = GlobalVariables.phoneNumberLimit
            let currentString: NSString = txtPhoneNumber.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCompnyName
        {
            self.view.endEditing(true)
//            companyNameDD.show()
            return false
        }
        
        if (textField == txtPhoneNumber) || (textField == txtUserName) {
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
}

//MARK: - TextView Delegate

extension SubmitDetailsVc : UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if textView == txtvwManufacture {
            self.lblPlaceholderManufacture.isHidden = textView.text == "" ? false : true
        }
        else if textView == txtvwIssueDescription
        {
            self.lblIssueDescriptionPlaceHolder.isHidden = textView.text == "" ? false:true
        }
        else if textView == txtvwLocationOfEquipment
        {
            self.lblLocationOfEquipment.isHidden = textView.text == "" ? false:true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: - CollectionView delegate and DataSource

extension SubmitDetailsVc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayImage.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultiSelectImgCollectionCell", for: indexPath) as! MultiSelectImgCollectionCell
            cell.btnAddPhoto.tag = indexPath.row
            cell.btnAddPhoto.addTarget(self, action: #selector(imgSelection), for: .touchUpInside)
        
        cell.btnRemoveImg.tag = indexPath.row
        
        cell.btnRemoveImg.addTarget(self, action: #selector(btnRemoveImage), for: .touchUpInside)
        
            if(self.arrayImage.count == indexPath.row)
            {
                cell.btnAddPhoto.isHidden = false
                cell.btnRemoveImg.isHidden = true
                cell.img.image = UIImage(named: "ic_spalsh_holder_add_image")
//                cell.img.image = UIImage(named:"ic_image_add_splash_holder")
                
            }
            else
            {
                cell.btnAddPhoto.isHidden = true
                cell.btnRemoveImg.isHidden = false
                cell.img.image = self.arrayImage[indexPath.row]
            }
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
       // let width = (collectionView.frame.size.width/2)
       // print("collectionView.frame.size.width:\(collectionView.frame.size.width)")
        return CGSize(width: 200.0 , height:128.0)
    }
    
}

//MARK:- TOCropViewcontroller Delegate

extension SubmitDetailsVc:TOCropViewControllerDelegate
{
    func openCropVC(Image:UIImage)
    {
        let cropVC = TOCropViewController(image: Image)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: 250)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        self.present(cropVC, animated: false, completion: nil)
        
    }
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        // self.imgUserPro.image = image
        
        self.arrayImage.append(image)
        self.collectionImage.reloadData()
        
        let indexPath = IndexPath(row: self.arrayImage.count, section: 0)
        self.collectionImage.scrollToItem(at: indexPath, at: .right, animated: false)
        
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        // isHaveImage = false
        dismiss(animated: true, completion: nil)
    }
}

//MARK: CurrentLocation Delegate
extension SubmitDetailsVc : CurrentLocationDelegate {
    
    func didUpdateLocation(lat: Double!, lon: Double!) {
        
        self.lat = lat
        self.long = lon
        
        print("Lotitude:",lat ?? 0.0)
        print("Longitude:",lon ?? 0.0)
        
    }
}


//MARK: - API calling

extension SubmitDetailsVc
{
    
    func submitDetailsAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicURLForClient)\(issueDetailURL)\(submitDetailsURL)"
            
            print("URL: \(url)")
            
            var param = ["client_name" : txtUserName.text ?? "",
                         "contact_no" : txtPhoneNumber.text ?? "",
                         "issue_description" : txtvwIssueDescription.text ?? "",
                         "company_id" : "\(getUserDetail("company_id"))",
                         "computer_name" : self.txtEquipmentName.text ?? "",
                         "manufacturer" : self.txtvwManufacture.text ?? "",
                         "location" : self.txtvwLocationOfEquipment.text ?? "",
                         "lattitude": "\(appdelgate.lattitude ?? 0.0)",
                         "longitude": "\(appdelgate.longitude ?? 0.0)",
                         "user_id":getUserDetail("id"),
                         "alt_contact_no": self.txtAlternatePhoneNumber.text ?? "",
                         "ext_contact_no": self.txtExt.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            var UplodedImg : [JSON] = []
            for i in 0..<arrayImage.count
            {
                var UplodedImgDic = JSON()
                UplodedImgDic["image"].stringValue = "file\(i+1)"
                UplodedImg.append(UplodedImgDic)
            }
            
            print("uploadImags:\(UplodedImg)")
            
            param["picture"] = JSON(UplodedImg).rawString()
            
            let PasswordString =  String(format: "\(kBasicUsername):\(kBasicPassword)")
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            print("param:==\(param)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for i in 0..<self.arrayImage.count
                {
                    let imgData = self.arrayImage[i].jpegData(compressionQuality: 0.7)
                    
                    multipartFormData.append(imgData!, withName: "file\(i+1)", fileName:"images\(i+1)" , mimeType: "image/jpg")
                    
                }
                
                for (key, value) in param {
                    multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, usingThreshold: unit64, to: url, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        
                        //stop
                        self.stopAnimating()
                        
                        print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                            {
                                
                                
                                let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SuccessPopVc") as! SuccessPopVc
                                
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                /*
                                
                                let alert = UIAlertController(title: getCommonString(key: "Draco_Support_key") , message: json["msg"].stringValue, preferredStyle: UIAlertController.Style.alert)
                                
                                alert.addAction(UIAlertAction(title: getCommonString(key: "Done_key"), style: UIAlertAction.Style.default, handler: { action in
                                    
                                    self.navigationController?.popViewController(animated: true)
                                    
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                */
                            }
                            else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                            {
//                                makeToast(strMessage: json["msg"].stringValue)
                            }
                        }
                        else
                        {
                            makeToast(strMessage: serverNotResponding)
                        }
                        
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    makeToast(strMessage: serverNotResponding)
                    
                }
            })
            
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func getCompanyOrganizationList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(adminListURL)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyName = data
                        
                        self.arrayCompanyNameString = data.map({"\($0["company_name"].stringValue)"})
                        self.companyNameDD.dataSource = self.arrayCompanyNameString
                        
                        //Set default when enter in the screen
                        if self.selectedController == .withScanOrNumber
                        {
                            self.strCompnayID = self.scanData["company_id"].stringValue
                            
                            var strCompnayName = ""
                            
                            if self.arrayCompanyName.contains(where: { (json) -> Bool in
                                
                                if json["id"].stringValue == self.strCompnayID
                                {
                                    strCompnayName = json["company_name"].stringValue
                                    return true
                                }
                                
                                return false
                            })
                            {
                                print("strCompanyName : \(strCompnayName)")
//                                self.txtCompnyName.text = strCompnayName
                            }
                            
//                            self.txtEquipmentName.text = self.scanData["computer_name"].stringValue
//                            self.txtvwLocationOfEquipment.text = self.scanData["location"].stringValue
//                            self.txtvwManufacture.text = self.scanData["manufacturer"].stringValue
//                            self.lblPlaceholderManufacture.isHidden = true
//                            self.lblLocationOfEquipment.isHidden = true
                            
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}



