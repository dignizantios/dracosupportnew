//
//  AppDelegate.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//


// com.app.Draco-Support


import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
import GoogleMaps
import GooglePlaces
import CoreLocation
import SJSwiftSideMenuController
import AlamofireSwiftyJSON
import Alamofire


protocol CurrentLocationDelegate {
    func didUpdateLocation(lat:Double!,lon:Double!)
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var lattitude:Double?
    var longitude:Double?
    var objCurrentLocationDelegate:CurrentLocationDelegate?
    var objCustomTabBar = CustomTabbarVC()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        //Redirect when click on push and login that time set true
        isScanScreenForIssue = true
        setUpQuickLocationUpdate()
        //Push notification
        
        FirebaseApp.configure()
        
        application.applicationIconBadgeNumber = 0
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.removeAllDeliveredNotifications()
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
                
                if granted {
                    print("granted:==\(granted)")
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "grantedFalse"), object: nil)
                }
                
            }
        }
        else
        {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            
            UIApplication.shared.registerForRemoteNotifications()
        }

        if getUserDetail("id") != "" {
            
//            self.uncheckedNotificationAPICalling()
            if getUserDetail("role") != "3" {
                appdelgate.sideBarSetUp()
            }
        }

        //Map Integration---  AIzaSyC1WUEDlcogOOomYbsG-KJ-G2g7jxyxwyA
        GMSServices.provideAPIKey("AIzaSyC1WUEDlcogOOomYbsG-KJ-G2g7jxyxwyA")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if getUserDetail("id") != "" {
            
            self.uncheckedNotificationAPICalling()
//            if getUserDetail("role") != "3" {
//                appdelgate.sideBarSetUp()
//            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    func sideBarSetUp() {
        
        let mainVC = SJSwiftSideMenuController()
        
        let slideLeft : SlideMenuVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SlideMenuVC") as! SlideMenuVC
        
        let slideRight : SlideMenuVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SlideMenuVC") as! SlideMenuVC
        
        let loginVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminLoginVc") as! AdminLoginVc  //RootVC
        
        let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController  //RootVC
        
//        appdelgate.objCustomTabBar = CustomTabbarVC() //RootVC
        
        if getUserDetail("id") != "" {
            if getUserDetail("role") == "3" {
//                SJSwiftSideMenuController.setUpNavigation(rootController: vc, leftMenuController: slideLeft, rightMenuController: slideRight, leftMenuType: .SlideView, rightMenuType: .SlideView)
            }
            else {
                SJSwiftSideMenuController.setUpNavigation(rootController: loginVC, leftMenuController: slideLeft, rightMenuController: slideRight, leftMenuType: .SlideView, rightMenuType: .SlideView)
            }
        }
        
//        SJSwiftSideMenuController.setUpNavigation(rootController: loginVC, leftMenuController: slideLeft, rightMenuController: slideRight, leftMenuType: .SlideView, rightMenuType: .SlideView)
        
//        SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .LEFT)
        SJSwiftSideMenuController.enableDimbackground = true
//        SJSwiftSideMenuController
        SJSwiftSideMenuController.leftMenuWidth = UIScreen.main.bounds.width-80
        
        self.window?.rootViewController = mainVC
        self.window?.makeKeyAndVisible()
    }
}


//MARK: - Usernotification delegate

extension AppDelegate
{
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error.localizedDescription)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let dict = JSON(notification.request.content.userInfo)
        
        print("Unchecked Notification:",dict["unseen_notification"].stringValue)
        print("Dict : \(JSON(dict))")
        
        GlobalVariables.badgeCount = dict["unseen_notification"].stringValue
        
        print(appdelgate.objCustomTabBar.selectedIndex)
        if appdelgate.objCustomTabBar.selectedIndex == 0 {
            NotificationCenter.default.post(name: Notification.Name("notificationGetNewRequest"), object: nil)
        }
        else if appdelgate.objCustomTabBar.selectedIndex == 1  {
            NotificationCenter.default.post(name: Notification.Name("notificationGetInProgress"), object: nil)
        }
        else if appdelgate.objCustomTabBar.selectedIndex == 2 {
            NotificationCenter.default.post(name: Notification.Name("notificationGetHistory"), object: nil)
        }
        else {
            NotificationCenter.default.post(name: Notification.Name("notificationGet"), object: nil)
        }
        
//        NotificationCenter.default.post(name: Notification.Name("notificationGetRequest"), object: nil)
        /*
        if isRequestList == true
        {
             completionHandler([.alert,.badge,.sound])
        }
        else
        {
            completionHandler([.alert,.badge,.sound])
        }*/
        
         completionHandler([.alert,.badge,.sound])
        
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        
        let jsonDict = response.notification.request.content.userInfo
        
        print("JSON DICt - ",JSON(jsonDict))
        GlobalVariables.notificationCount = jsonDict["unseen_notification"] as? String ?? "0"
        if getUserDetail("id") == ""
        {
            return
        }
        
        print("isScanScreenForIssue:\(isScanScreenForIssue)")
        
        if isScanScreenForIssue == true
        {
            
      //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ScanScreenForIssue"), object: nil)
            
            appdelgate.objCustomTabBar = CustomTabbarVC()
            let navigationController = UINavigationController(rootViewController: appdelgate.objCustomTabBar)
            navigationController.popToRootViewController(animated: false)
            self.window?.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }
        else if isRequestList == true
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshRequestList"), object: nil)
        }
        else
        {
            appdelgate.objCustomTabBar.selectedIndex = 0
        }
        
        
        completionHandler()
    }
    
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    
    func connectToFcm() {
        // Won't connect since there is no token
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        
    }
    
}

//MARK:- Location Delegate

extension AppDelegate:CLLocationManagerDelegate
{
    func setUpQuickLocationUpdate()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        locationManager.activityType = .automotiveNavigation
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.distanceFilter = 10.0
        //        locationManager.requestAlwaysAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let latestLocation = locations.first
        {
            
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            //  KSToastView.ks_showToast("Location Update Successfully", duration: ToastDuration)
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                
                GlobalVariables.userCurrentLocation = CLLocation(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude)
                self.lattitude = latestLocation.coordinate.latitude
                self.longitude = latestLocation.coordinate.longitude
                self.objCurrentLocationDelegate?.didUpdateLocation(lat:self.lattitude,lon:self.longitude)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //    manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                self.openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                self.openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Location Manager failed with the following error: \(error)")
    }
    //#MARK:- Manage the location regione
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("x with identifier: \(region.identifier)")
        print("The monitored regions are: \(manager.monitoredRegions)")
    }
    func openSetting()
    {
        let alertController = UIAlertController (title: getCommonString(key: "Rent_a_sky_key"), message: getCommonString(key: "Location_Allow_from_settings_key"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: getCommonString(key: "Settings_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        
        
        //             let cancelAction = UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .default, handler: nil)
        //            alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}

//MARK: Notification get count
extension AppDelegate {
    
    //MARK: Unseen notification
    func uncheckedNotificationAPICalling() {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(userURL)\(unseen_notification)"
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("id")]
            print("param :\(param)")
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    GlobalVariables.badgeCount = json["unseen_notification"].stringValue
                    print("Notification Count:", json["unseen_notification"].stringValue)
                    print(appdelgate.objCustomTabBar.selectedIndex)
                    if appdelgate.objCustomTabBar.selectedIndex == 0 {
                        NotificationCenter.default.post(name: Notification.Name("notificationGetNewRequest"), object: nil)
                    }
                    else if appdelgate.objCustomTabBar.selectedIndex == 1  {
                        NotificationCenter.default.post(name: Notification.Name("notificationGetInProgress"), object: nil)
                    }
                    else if appdelgate.objCustomTabBar.selectedIndex == 2 {
                        NotificationCenter.default.post(name: Notification.Name("notificationGetHistory"), object: nil)
                    }
                    else {
                        NotificationCenter.default.post(name: Notification.Name("notificationGet"), object: nil)
                    }
                    /*
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        GlobalVariables.badgeCount = json["unseen_notification"].stringValue
                        print("Notification Count:", json["unseen_notification"].stringValue)
                    }
                     */
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}

