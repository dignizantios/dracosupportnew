//
//  UIViewController+Extension.swift
//  JoeToGo
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import DropDown
import SideMenu
import SJSwiftSideMenuController

extension UIViewController : NVActivityIndicatorViewable {
    
    
    func addDoneButtonOnKeyboard(textfield : UITextField) {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appthemeRedColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: getCommonString(key: "Done_key"), style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    //MARK:- Navigation Bar Setup
    
    func setUpNavigationBarWithTitle(strTitle : String,isRightButtonHidden:Bool) {
      
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appthemeRedColor
        self.navigationController?.navigationBar.isTranslucent = false
        
       setNavigationShadow()
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .semibold)
        
        self.navigationItem.titleView = HeaderLabel
        self.navigationItem.hidesBackButton = true
        
        /*
        if !isRightButtonHidden {
            let rightButton = UIBarButtonItem(title: getCommonString(key: "Logout"), style: .plain, target: self, action: #selector(LogoutButtonAction))
            
//            let rightButton = UIBarButtonItem(image: UIImage(named: "ic_logout"), style: .plain, target: self, action: #selector(LogoutButtonAction))
            rightButton.setTitleTextAttributes([NSAttributedString.Key.font: themeFont(size: 16, fontname: .regular)], for: .normal)
            rightButton.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItem = rightButton
        }
 */
 
//        self.slideMenuSetUp()
        if getUserDetail("id") != "" {
            
            let notificationButton = UIBarButtonItem(image: UIImage(named: "ic_notification"), style: .plain, target: self, action: #selector(showNotificationVC))
            notificationButton.tintColor = .white
            
            if (getUserDetail("role") == "3") {
                
//                self.navigationItem.leftBarButtonItem = notificationButton
                
                if !isRightButtonHidden
                {
                    let rightButton = UIBarButtonItem(title: getCommonString(key: "Logout"), style: .plain, target: self, action: #selector(LogoutButtonAction))
                    
                    //            let rightButton = UIBarButtonItem(image: UIImage(named: "ic_logout"), style: .plain, target: self, action: #selector(LogoutButtonAction))
                    rightButton.setTitleTextAttributes([NSAttributedString.Key.font: themeFont(size: 16, fontname: .regular)], for: .normal)
                    rightButton.tintColor = UIColor.white
                    self.navigationItem.rightBarButtonItem = rightButton
                }
            }
            else {
                
//                let leftButton = UIBarButtonItem(image: UIImage(named: "ic_setting"), style: .plain, target: self, action: #selector(btnSlideAction))
                let leftButton = UIBarButtonItem(image: UIImage(named: "ic_setting"), style: .plain, target: self, action: #selector(sideBarShow))
                leftButton.tintColor = .white
                self.navigationItem.leftBarButtonItem = leftButton
                
                if !isRightButtonHidden {
//                    self.navigationItem.rightBarButtonItem = notificationButton
                }
            }
        }
    }
    
    @objc func sideBarShow() {
//        SJSwiftSideMenuController.showLeftMenu()
        SJSwiftSideMenuController.toggleLeftSideMenu()
    }
    
    func setUpNavigationBarWhiteWithTitleAndBack(strTitle : String) {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appthemeRedColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationItem.hidesBackButton = true
        
        setNavigationShadow()
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .semibold)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    
    func setNavigationShadow()
    {
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func btnBackAction(_ sender : UIButton)
    {
        backButtonAction()
    }
    
    @objc func LogoutButtonAction()
    {
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_want_to_logout_?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setUpNavigationLeftQRCode() {
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_qr_code_white_icon"), style: .plain, target: self, action: #selector(QRCodeSetupScreen))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = true
    }
    
    
    @objc func setUpBadgeCountAndBarButton() {
        // badge label
        let label = UILabel(frame: CGRect(x: 8, y: -10, width: 18, height: 18))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.font = themeFont(size: 10, fontname: .regular)
        label.backgroundColor = .black
        label.text = GlobalVariables.badgeCount
        
        label.isHidden = GlobalVariables.badgeCount == "0" ? true : false
        
        // button
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 15, height: 18))
        rightButton.setBackgroundImage(UIImage(named: "ic_notification"), for: .normal)
        rightButton.addTarget(self, action: #selector(showNotificationVC), for: .touchUpInside)
        rightButton.addSubview(label)
        
        // Bar button item
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
        
        if getUserDetail("id") != "" {
            if (getUserDetail("role") == "3") {
                navigationItem.leftBarButtonItem = rightBarButtomItem
            }
            else {
                navigationItem.rightBarButtonItem = rightBarButtomItem
            }
        }
    }
    
    
    
    @objc func QRCodeSetupScreen() {
        let qrCodeSetup = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanForCodeSetup") as! ScanForCodeSetup
        self.navigationController?.pushViewController(qrCodeSetup, animated: false)
    }
    
    @objc func showNotificationVC() {
        
        let notificationVC = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        
        notificationVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
   
    //MARK: - Loader
    
    
    func showLoader()
    {
        //let LoaderString:String = "Loading..."
        let LoaderSize = CGSize(width: 50, height: 50)
        
        startAnimating(LoaderSize, message: nil, type: NVActivityIndicatorType.ballPulseSync)
        
    }
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }

    //MARK: - DropDown Config
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .bottom
        dropdown.dismissMode = .onTap
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
        
    }
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = OrignalFormatter
        guard let convertedDate = dateformatter.date(from: strDate) else {
            return ""
        }
        dateformatter.dateFormat = YouWantFormatter
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
    //MARK:- key board hide
    
    func hidekeyBoardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
}
